<div class="clearfix footer_bg">
    <div class="footer_section">
		<div class="container">
			<div class="row footer_top">
				<div class="col-md-12 col-sm-12">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1') ) : ?>
					<?php endif; ?>
				</div>
		    </div>
		</div>
	</div>
	<?php $wl_theme_options = weblizar_get_options(); ?>
	<div class="footer_section footer_below">
		<div class="container">
		   <div class="row ">
		    	<div class="col-md-12 col-sm-12">
		    		<div class="col-md-10 col-sm-10 col-xs-12">
		    			<p>
						<span><?php if($wl_theme_options['footer_copyright']) { echo esc_attr($wl_theme_options['footer_copyright']); }?></span>
						&nbsp;
						<span><a style="color:#f0f0f0;" href="http://www.beian.miit.gov.cn/" target="_blank">赣ICP备16010772号</a></span>
						&nbsp;
						<span>版權所有：<?php if($wl_theme_options['footer_copyright_owner']) { echo esc_attr($wl_theme_options['footer_copyright_owner']); }?></span>
						</p><br/>
		    			<p>地址：<?php if($wl_theme_options['footer_address']) { echo esc_attr($wl_theme_options['footer_address']); }?></p>
		    			<p><span>銀行賬號：<?php if($wl_theme_options['footer_bank_account']) { echo esc_attr($wl_theme_options['footer_bank_account']); }?></span>&nbsp;<span>戶名：<?php if($wl_theme_options['footer_account_name']) { echo esc_attr($wl_theme_options['footer_account_name']); }?></span></p>
		    			<p><span>客堂电话：<?php if($wl_theme_options['footer_tel_number']) { echo esc_attr($wl_theme_options['footer_tel_number']); }?></span>&nbsp;<span>Email：<a href="mailto:<?php if($wl_theme_options['footer_email']) { echo esc_attr($wl_theme_options['footer_email']); }?>"><?php if($wl_theme_options['footer_email']) { echo esc_attr($wl_theme_options['footer_email']); }?></a></span></p>
						
		    		</div>
		    		<div class="col-md-2 col-sm-2 col-xs-12">
		    			<div class="footer_erwei">
		    				<img src="<?php bloginfo('template_url');?>/images/erweima_200.jpg"/>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		</div>
	</div>
</div>

<a href="#" title="Go Top" class="enigma_scrollup" style="display: inline;"><i class="fa fa-chevron-up"></i></a>
<?php if($wl_theme_options['custom_css']) ?>
<style type="text/css">
<?php { echo esc_attr($wl_theme_options['custom_css']); } ?>
</style>
<?php wp_footer(); ?>




</body>
</html>