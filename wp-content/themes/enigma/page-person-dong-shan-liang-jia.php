<?php
	 /*
	 Template Name: 洞山良價专题模板
	 */
?>
<?php get_header();  ?>

<div class="clearfix neiye_banner">
	<div class="row">
		<?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			  the_post_thumbnail();
			}else{   ?>
			<img src="<?php bloginfo('template_url');?>/images/shanlirenjia.jpg"/>
		<?php }?>
	</div>
</div>

<div class="renwu">

<div class="clearfix renwu_nav_bg">
  <div class="container renwu_nav">
         <?php 
		       wp_nav_menu( array(
				'theme_location' => 'liangjia',
				'menu_class' => 'nav navbar-nav',
				'fallback_cb' => 'weblizar_fallback_page_menu',
				'walker' => new weblizar_nav_walker(),
				)
			);	
	     ?>				
  </div>
</div>

<div class="clearfix">
	<div class="container renwu_gaopeng" id="shengpingzhuanji">
		<div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>生平傳記</h5>
	    </div>
	    <div class="col-md-12">
		    <div class="col-md-5">
		    	<img src="<?php bloginfo('template_url');?>/images/renwu/liangjia.jpg"/>
		    </div>
		    <div class="col-md-7 renwu_gaopeng_list">
		    <?php
	           query_posts('cat=59&showposts=6&orderby=date&order=ASC');
	         ?>
			 <?php while (have_posts()) : the_post(); ?>	
				 <div class="renwu_gaopeng_lbox">
			    	<h6><a href="<?php the_permalink();?>"><?php the_title()?></a></h6>
				    <p><?php the_excerpt();?>...<a href="<?php the_permalink();?>" class="renwu_more">【查看详情】</a></p>
				</div>
	          <?php endwhile;?>
	         <?php wp_reset_query();?>
		    </div>
		</div>
	</div>
</div>

<div class="clearfix">
	<div class="container renwu_gaopeng" id="chanfengsixiang">
		<div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>禪風思想</h5>
	    </div>
	    <div class="col-md-12">
		    <div class="col-md-7 renwu_gaopeng_list">
		    <?php
	           query_posts('cat=60&showposts=8&orderby=date&order=ASC');
	         ?>
			 <?php while (have_posts()) : the_post(); ?>	
				 <div class="renwu_gaopeng_lbox">
			    	<h6><a href="<?php the_permalink();?>"><?php the_title()?></a></h6>
				    <p><?php the_excerpt();?>...<a href="<?php the_permalink();?>" class="renwu_more">【查看详情】</a></p>
				</div>
	          <?php endwhile;?>
	         <?php wp_reset_query();?>
		    </div>
		    <div class="col-md-5" style="text-align:center;">
		    	<img src="<?php bloginfo('template_url');?>/images/renwu/cishijie.jpg"/>
		    </div>
		</div>
	</div>
</div>

<div class="clearfix">
	<div class="container renwu_gaopeng" id="chizifoxin">
		<div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>赤子佛心</h5>
	    </div>
	    <div class="col-md-12">
		    <div class="col-md-5">
		    	<img src="<?php bloginfo('template_url');?>/images/renwu/chanshita.jpg"/>
		    </div>
		    <div class="col-md-7 renwu_gaopeng_list">
		    <?php
	           query_posts('cat=61&showposts=-1&orderby=date&order=ASC');
	         ?>
			 <?php while (have_posts()) : the_post(); ?>	
				 <div class="renwu_gaopeng_lbox">
			    	<h6><a href="<?php the_permalink();?>"><?php the_title()?></a></h6>
				    <p><?php the_excerpt();?>...<a href="<?php the_permalink();?>" class="renwu_more">【查看详情】</a></p>
				</div>
	          <?php endwhile;?>
	         <?php wp_reset_query();?>
		    </div>
		</div>
	</div>
</div>

<div class="clearfix">
  <div class="container" id="chanmenjingdian">
	  <div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>禪門經典</h5>
	  </div>
	  <div class="col-md-12">
	   <?php
           query_posts('cat=62&showposts=-1&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>	
		 <div class="col-md-6">
			  <div class="renwu_kuozhan renwu_floatL">
				  <div class="renwu_k_txt" style="width:96%;margin-right: 2%;">
				  	<a href="<?php the_permalink();?>"><h5><?php the_title();?></h5></a>
				  	<p><?php the_excerpt();?></p>...
				  	<a href="<?php the_permalink();?>" class="renwu_more">【查看詳情】</a>
				  </div>
			  </div>
		  </div>
          <?php endwhile;?>
         <?php wp_reset_query();?>
	  </div>
  </div>
</div>


<div class="clearfix">
  <div class="container" id="dangdaijiedu">
	  <div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>當代解讀</h5>
	  </div>
	  <div class="col-md-12">
	   <?php
           query_posts('cat=63&showposts=-1&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>	
		 <div class="col-md-6">
			  <div class="renwu_kuozhan renwu_floatL">
				  <div class="renwu_k_txt" style="width:96%;margin-right: 2%;">
				  	<a href="<?php the_permalink();?>"><h5><?php the_title();?></h5></a>
				  	<p><?php the_excerpt();?></p>...
				  	<a href="<?php the_permalink();?>" class="renwu_more">【查看詳情】</a>
				  </div>
			  </div>
		  </div>
          <?php endwhile;?>
         <?php wp_reset_query();?>
	  </div>
  </div>
</div>


</div>

<?php get_footer(); ?>

<script src="<?php bloginfo('template_url');?>/js/jquery.bxslider.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
	  jQuery('.bxslider').bxSlider({
	  	  slideWidth: 250,
	  	  minSlides: 4,
	  	  maxSlides: 4,
	  	  moveSlides: 1,
	  	  slideMargin: 5,
	  });
	});
</script>
<script type="text/javascript">
jQuery("#menu-menu-person li a").click(function(){
	var url = jQuery(this).attr("href");
	var id = url.split("#")[1];
    if(id){
        var t = jQuery("#"+id).offset().top-120;
        jQuery("html,body").animate({scrollTop:t},1000)
    }
    return false;
});
</script>
