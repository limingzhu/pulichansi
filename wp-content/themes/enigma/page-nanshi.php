<?php
	 /*
	 Template Name: 南师与洞山模板
	 */
?>
<?php get_header();  ?>
<div class="clearfix neiye_banner">
		<div class="row">
		<?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			  the_post_thumbnail();
			}else{   ?>
			<img src="<?php bloginfo('template_url');?>/images/ziguitiigui.jpg"/>
		<?php }?>
	</div>
</div>
<div class="clearfix">
  <div class="container">
    <div class="row">
		<div class="row neiye_warpper">
		    <?php get_sidebar(); ?>	
			<div class="col-md-9 col-sm-9 col-xs-12">
			  <div class="nanshi_video">
				  <?php the_field('nanshi_video');?>
			  </div>
			  <div class="nanshi_content">
			    <div class="nanshi_content_title">
					<h1><?php the_field('page_big_title');?></h1>
					<p><?php the_field('nanshi_author');?></p>
				</div>
				 <?php 
	                while ( have_posts() ) : the_post();
	                   the_content();
	                endwhile;
	             ?>
			  </div>
		    </div>
		</div>   
	</div>
  </div>	
</div>
<?php get_footer(); ?>