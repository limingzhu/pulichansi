<?php
	$root_cat = get_root_id($cat);   //获取到顶级分类
?>

<?php //子规啼归?>
<?php if(in_category(array(1,9))):?>
<div class="col-md-3 col-sm-3 col-xs-12 neiye_sidebar">
  <h2><?php echo get_category(1)->name;?></h2>
	<div class="sidebar-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="sidebar sidebar-default">
	    <div class="sidebar-heading" role="tab" id="headingOne">
	      <h4 class="sidebar-title">
	        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" <?php if(is_page(32)||is_page(28)){ echo "style='color:#99594c;'";}?>>
	        <label <?php if(is_page(32)||is_page(28)){ echo "style='background:url(/wp-content/themes/enigma/images/sp.png);'";}?>></label><?php echo get_category(9)->name;?>
	        </a>
	      </h4>
	    </div>
	    <div id="collapseOne" class="sidebar-collapse collapse <?php if(is_page(32)||is_page(28)){ echo 'in';}?>" role="tabpanel" aria-labelledby="headingOne">
	      <div class="sidebar-body">
		    <ul>
		      <?php 
		         $page_chongjian = get_posts('post_type=page&orderby=id&order=asc&showposts=-1&cat=9');
		         if(!empty($page_chongjian)): foreach ($page_chongjian as $page) : ?>
		         <li><a href="<?php echo get_permalink($page->ID);?>" <?php if(is_page($page->ID)){ echo "style='color:#99594c;'";}?>><?php echo $page->post_title;?></a></li>
		      <?php endforeach;endif; ?>
		    </ul>
	      </div>
	    </div>
	  </div>
	  <?php 
		  $page_zigui = get_posts("post_type=page&orderby=id&order=asc&showposts=-1&cat=1");
		  if(!empty($page_zigui)):	foreach ($page_zigui as $page): if($page->ID!=28&&$page->ID!=32):?>
	  		 <div class="sidebar sidebar-default">
			    <div class="sidebar-heading">
			      <h4 class="sidebar-title"><a href="<?php echo get_permalink($page->ID);?>"  <?php if(is_page($page->ID)){ echo "style='color:#99594c;'";}?>><label <?php if(is_page($page->ID)){ echo "style='background:url(/wp-content/themes/enigma/images/sp.png);'";}?>></label><?php echo $page->post_title;?></a></h4>
			    </div>
			  </div>
	 <?php endif;endforeach;endif; ?>
	</div>
</div>
<?php endif;?>

<?php //洞下之风?>
<?php if(in_category(array(2,10,11))):?>
<div class="col-md-3 col-sm-3 col-xs-12 neiye_sidebar">
  <h2><?php echo get_category(2)->name;?></h2>
	<div class="sidebar-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="sidebar sidebar-default">
	    <div class="sidebar-heading">
	      <h4 class="sidebar-title">
	        <a href="<?php echo get_category_link(10);?>" <?php if(is_category(10)||in_category(10)){ echo "style='color:#99594c;'";}?> >
	        <label <?php if(is_category(10)||in_category(10)){ echo "style='background:url(/wp-content/themes/enigma/images/sp.png);'";}?> ></label><?php echo get_category(10)->name;?>
	        </a>
	      </h4>
	    </div>
	  </div>
	  <?php 
		  $page_zigui = get_posts("post_type=page&orderby=id&order=asc&showposts=-1&cat=2");
		  if(!empty($page_zigui)):	foreach ($page_zigui as $page): if($page->post_type == 'page'): ?>
	  		 <div class="sidebar sidebar-default">
			    <div class="sidebar-heading">
			      <h4 class="sidebar-title"><a href="<?php echo get_permalink($page->ID);?>"  <?php if(is_page($page->ID)){ echo "style='color:#99594c;'";}?>><label <?php if(is_page($page->ID)){ echo "style='background:url(/wp-content/themes/enigma/images/sp.png);'";}?>></label><?php echo $page->post_title;?></a></h4>
			    </div>
			  </div>
	 <?php endif; endforeach; endif; ?>
	</div>
</div>
<?php endif;?>

<?php //睹影悟道?>
<?php if($root_cat==3):?>
<div class="col-md-3 col-sm-3 col-xs-12 neiye_sidebar">
  <h2><?php echo get_category(3)->name;?></h2>
	<div class="sidebar-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="sidebar sidebar-default">
	    <div class="sidebar-heading">
	      <h4 class="sidebar-title">
		      <?php $hdyg_id = 12;?>
		      <a <?php if(is_category($hdyg_id)||in_category($hdyg_id)){ echo "style='color:#99594c;'";}?> href="<?php echo get_category_link($hdyg_id);?>">
			      <label <?php if(is_category($hdyg_id)||in_category($hdyg_id)){ echo "style='background:url(/wp-content/themes/enigma/images/sp.png);'";}?>></label>
			      <?php echo get_category($hdyg_id)->name;?>
		      </a>
	      </h4>
	    </div>
	  </div>
	  <div class="sidebar sidebar-default">
	    <div class="sidebar-heading">
	      <h4 class="sidebar-title">
	          <?php $cgzs_id = 13;?>
		      <a <?php if(is_category($cgzs_id)||in_category($cgzs_id)){ echo "style='color:#99594c;'";}?> href="<?php echo get_category_link($cgzs_id);?>">
			      <label <?php if(is_category($cgzs_id)||in_category($cgzs_id)){ echo "style='background:url(/wp-content/themes/enigma/images/sp.png);'";}?>></label>
			      <?php echo get_category($cgzs_id)->name;?>
		      </a>
	      </h4>
	    </div>
	  </div>
	  <div class="sidebar sidebar-default">
	    <div class="sidebar-heading"  role="tab" id="headingOne">
	      <h4 class="sidebar-title">
		      <?php $dywh_id = 14;?>
		      <a <?php if(is_category($dywh_id)||in_category($dywh_id)){ echo "style='color:#99594c;'";}?> href="<?php echo get_category_link($dywh_id);?>">
			      <label <?php if(is_category($dywh_id)||in_category($dywh_id)){ echo "style='background:url(/wp-content/themes/enigma/images/sp.png);'";}?>></label>
			      <?php echo get_category($dywh_id)->name;?>
		      </a>
	      </h4>
	    </div>
		  <div id="collapseOne" class="sidebar-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      <div class="sidebar-body">
			    <ul>
			    <?php  
			    $category= get_the_category();
			    $fenLeiID = get_category_children(14);
				$fenLeiID = explode('/',$fenLeiID);
		        for($i=0;$i<count($fenLeiID);$i++){ 
  			     	$Infor = get_category($fenLeiID[$i]);
  			     	//print_r($Infor);
  			     	if($Infor->name != ""){?>
				  <li>
				  	<a href="<?php echo get_category_link($fenLeiID[$i]);?>"
				  		<?php  if($Infor->term_id==$cat||$Infor->term_id==$category[0]->term_id){
				  			      echo "style='color:#99594c'";
				  			  }?>>
				  	<?php echo "$Infor->name"; ?></a> 
				  </li>
				<?php } } ?>
			    </ul>
		      </div>
		   </div>
	   </div>
	  <div class="sidebar sidebar-default">
	    <div class="sidebar-heading"  role="tab" id="headingTwo">
	      <h4 class="sidebar-title">
	       <!--<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseTwo">-->
	         <?php $ffzj = 15;?>
		      <a <?php if(is_category($ffzj)||in_category($ffzj)){ echo "style='color:#99594c;'";}?> href="<?php echo get_category_link($ffzj);?>">
			      <label <?php if(is_category($ffzj)||in_category($ffzj)){ echo "style='background:url(/wp-content/themes/enigma/images/sp.png);'";}?>></label>
			      <?php echo get_category($ffzj)->name;?>
		      </a>
	      </h4>
	    </div>
	    <div id="collapseTwo" class="sidebar-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
		      <div class="sidebar-body">
			    <ul>
				    <?php  
				    $category= get_the_category();
				    $fenLeiID = wp_list_categories("child_of=15&depth=1&hide_empty=0&title_li=&orderby=id&order=ASC");
				     //print_r($fenLeiID);
					$fenLeiID = explode('/',$fenLeiID);
			        for($i=0;$i<count($fenLeiID);$i++){ 
	  			     	$Infor = get_category($fenLeiID[$i]);
	  			     	//print_r($Infor);
	  			     	if($Infor->name != ""){?>
					  <li>
					  	<a href="<?php echo get_category_link($fenLeiID[$i]);?>"
					  		<?php  if($Infor->term_id==$cat||$Infor->term_id==$category[0]->term_id){
					  			      echo "style='color:#99594c'";
					  			  }?>>
					  	<?php echo "$Infor->name"; ?></a> 
					  </li>
					<?php } } ?>
			    </ul>
		      </div>
		 </div>
	  </div>
	</div>
</div>
<?php endif;?>



<?php //山里人家?>
<?php if($root_cat==4||in_category(39)||$root_cat==48):?>
<div class="col-md-3 col-sm-3 col-xs-12 neiye_sidebar">
  <h2><?php echo get_category(4)->name;?></h2>
	<div class="sidebar-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <?php 
	   $categories = get_categories(array('parent' => 4,'hide_empty' => 0,'orderby'=>term_id,'order'=>asc));
	   $category_cur = get_the_category();
        foreach ($categories as $key => $value) :
          if($cat == $value->term_id|| get_category($cat)->category_parent == $value->term_id||$category_cur[0]->term_id == $value->term_id):  ?>
			  <div class="sidebar sidebar-default">
			    <div class="sidebar-heading">
			      <h4 class="sidebar-title"><a  style='color:#99594c;' href="<?php echo get_category_link($value->term_id);?>">
			       <label style='background:url(/wp-content/themes/enigma/images/sp.png);'></label><?php echo $value->cat_name;?></a>
			       </h4>
			    </div>
			  </div>
       <?php else: ?>
            <div class="sidebar sidebar-default">
			    <div class="sidebar-heading">
			      <h4 class="sidebar-title"><a href="<?php echo get_category_link($value->term_id);?>"><label></label><?php echo $value->cat_name;?></a></h4>
			    </div>
			  </div>
       <?php endif; endforeach;  ?>
        <div class="sidebar sidebar-default">
		    <div class="sidebar-heading">
		      <h4 class="sidebar-title"><a href="/person-nan-huai-jin"><label></label>南師懷瑾</a></h4>
		    </div>
		 </div>
		 <div class="sidebar sidebar-default">
		    <div class="sidebar-heading">
		      <h4 class="sidebar-title"><a href="/person-liang-jia"><label></label>洞山良價</a></h4>
		    </div>
		 </div>
	</div>
</div>
<?php endif;?>


<?php //义工招募?>
<?php if(is_page(345)):  //判断跟是否为普利行愿?>
<div class="col-md-3 col-sm-3 col-xs-12 neiye_sidebar">
  <h2><?php echo get_category(5)->name;?></h2>
	<div class="sidebar-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="sidebar sidebar-default">
		    <div class="sidebar-heading">
		      <h4 class="sidebar-title">
			      <a href="/volunteer/"  style='color:#99594c;'>
				      <label style='background:url(/wp-content/themes/enigma/images/sp.png);'></label>義工招募
			      </a>
		      </h4>
		    </div>
		</div>
	</div>
</div>
<?php endif;?>