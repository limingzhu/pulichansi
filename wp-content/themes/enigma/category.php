<?php
    //文件下载跳转设置
    if(is_category(40)){
      $url=bloginfo('template_url')."/category/shanlirenjia/xiazai/shuji/";
       Header("Location: $url");
    }
?>

<?php get_header(); ?>
<?php
	$root_cat_id = get_root_id($cat);   //获取到顶级分类
?>
<div class="clearfix neiye_banner">
	<div class="row">
		<?php
			if($root_cat_id==3){?>
			<img src="<?php bloginfo('template_url');?>/images/duyingwudao.jpg"/>
		<?php }else if($root_cat_id==4){?>
			<img src="<?php bloginfo('template_url');?>/images/shanlirenjia.jpg"/>
		<?php }else if($root_cat_id==2){?>
			<img src="<?php bloginfo('template_url');?>/images/dongxiazhifeng.jpg"/>
		<?php }?>
	</div>
</div>
<div class="clearfix">
  <div class="container">
    <div class="row">
		<div class="row neiye_warpper">
		    <?php get_sidebar(); ?>	
			<div class="col-md-9 col-sm-9 col-xs-12">
			    <?php 
			        //因为query_post语句单独显示会影响分页效果，所以改为以下代码
		            $limit = get_option('posts_per_page');
		            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			        if($root_cat_id==3||is_category(10)){                                     //睹影悟道栏目,曹洞宗門欄目
			        	query_posts('cat='.$cat.'&orderby=date&order=desc&posts_per_page=10&paged='.$paged);
			        	//内容在category-list.php页面
			            $wp_query->is_archive = true; $wp_query->is_home = false;
					    get_template_part('category-list', get_post_format() ); 
			        }else if($root_cat_id==4){                               //山里人家栏目
			        	if(is_category(39)){                                 //学习心得
			        		query_posts('cat='.$cat.'&orderby=date&order=desc&posts_per_page=10&paged='.$paged);
			        	}else if(is_category(41)||is_category(42)){          //书籍下载与音频下载
			        		query_posts('cat='.$cat.'&orderby=date&order=asc&posts_per_page=8&paged='.$paged);
			        	}else if(is_category(43)){                            //视频下载
			        		query_posts('cat='.$cat.'&orderby=date&order=asc&posts_per_page=6&paged='.$paged);
			        	}else{                                                //生活剪影与四季风光
			        		query_posts('cat='.$cat.'&orderby=date&order=desc&posts_per_page=9&paged='.$paged);
			        	}
			        	//内容在category-shanli.php页面
			            $wp_query->is_archive = true; $wp_query->is_home = false;
					    get_template_part('category-shanli', get_post_format() ); 
			        }
		            
		        ?>


		        <?php 
			        if(is_category(14)||is_category(15)):         //多元文化列表頁，佛法珠璣列表頁，無分頁
			        $all_cat =  get_categories(array('parent' => $cat,'hide_empty' => 0,'orderby'=>term_id,'order'=>asc));
		        ?>
				<div class="neiye_dywh">
					<?php foreach ($all_cat as $key => $cur_cat) : ?>
					<?php query_posts('cat='.$cur_cat->term_id.'&orderby=date&order=asc&showposts=6');?>
						<div class="neiye_dywh_box  col-md-6 col-sm-6 col-xs-12">
					      <div class="dywh_box">
					      <div class="dywh_box_title">
					      <?php if($cat==14):?>
					         <img src="<?php bloginfo('template_url');?>/images/gywh_t<?php echo $key+1;?>.png"/>
					      <?php endif; ?>
					      <?php if($cat==15):?>
					      	 <img src="<?php bloginfo('template_url');?>/images/ffzj_t<?php echo $key+1;?>.png"/>
					      <?php endif;?>
					         <a <?php if($cat==15){ echo "style='color:#fff';";}?> href="<?php echo get_category_link( $cur_cat->term_id ); ?>">更多&gt;&gt;</a>
					      </div>
					        <ul class="neiye_list_ul">   
					          <?php while (have_posts()) : the_post(); ?>      
						        <li>
						          <label>·</label>
						          <span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><label><?php the_time('m-d'); ?></label></span>
						        </li>
						      <?php endwhile;?>
					        </ul>
					      </div>
					   </div>
					<?php endforeach; ?>
				</div>
				<?php endif;?>


			</div>
		</div>
	</div>
  </div>
</div>
<?php get_footer(); ?>