<?php if(is_category(37)):     //生活剪影?>
<div class="neiye_shjy">
   <?php while (have_posts()) : the_post();  ?>  
          <div class="neiye_shjy_box col-md-4 col-sm-4 col-xs-6">  
              <a href="javascript:0;" class="cd-popup-trigger<?php the_ID();?>">  
                 <div class="shjy_one">
                    <div class="shjy_one_img">
                       <?php
                       $sh_spic_value = pods_field_display('mountain_life', $post->ID, 'mountain_life_spic'); 
                       if(!empty($sh_spic_value)):?>
                          <img src="<?php echo pods_field_display('mountain_life', $post->ID, 'mountain_life_spic');?>" alt="<?php the_title(); ?> " title="<?php the_title(); ?> "/>
                       <?php else: ?>
                         <img src="<?php echo pods_field_display('mountain_life', $post->ID, 'mountain_life_big_pic');?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?> "/>
                       <?php endif; ?>
                     </div>
                    <div class="shjy_one_txt"><?php the_title(); ?></div>
                 </div>
              </a>  
              <div class="cd-popup cd-popup<?php the_ID(); ?>">
                <div class="cd-popup-container">
                    <div class="shjy_box_big">
                        <img src="<?php echo pods_field_display('mountain_life', $post->ID, 'mountain_life_big_pic');?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?> "/>
                    </div>
                    <a href="#0" class="shjy_box_close cd-popup-close<?php the_ID(); ?>"></a>
                </div>
            </div>
          </div>  
  <?php endwhile;?>
<?php  weblizar_pagination();?>
</div>
 <script type="text/javascript">
  /*弹框JS内容*/
  jQuery(document).ready(function($){
    <?php while (have_posts()) : the_post();  ?>
      $('.cd-popup-trigger<?php the_ID();?>').on('click', function(event){
            //console.log("lalalal:",<?php echo $key;?>);
            event.preventDefault();
            $('.cd-popup<?php the_ID();?>').addClass('is-visible');
            console.log("addClass",".cd-popup<?php echo the_ID();?>");
            //$(".dialog-addquxiao").hide()
          });
          //关闭窗口
          $('.cd-popup<?php the_ID();?>').on('click', function(event){
              if( $(event.target).is('.cd-popup-close<?php the_ID();?>') || $(event.target).is('.cd-popup<?php echo the_ID();?>') ) {
                  event.preventDefault();
                  $(this).removeClass('is-visible');
              }
          });
          //ESC关闭
          $(document).keyup(function(event){
              if(event.which=='27'){
                  $('.cd-popup<?php the_ID();?>').removeClass('is-visible');
              }
          });
   <?php endwhile;?>
 });
</script>
<?php endif;?>

<?php if(is_category(38)):    //四季风光?>
<div class="neiye_shjy">
   <?php while (have_posts()) : the_post();  ?>  
          <div class="neiye_shjy_box col-md-4 col-sm-4 col-xs-6">  
              <a href="javascript:0;" class="cd-popup-trigger<?php the_ID();?>">  
                 <div class="shjy_one">
                    <div class="shjy_one_img">
                       <?php 
                       $sj_spic_value = pods_field_display('four_seasons', $post->ID, 'four_seasons_spic'); 
                       if(!empty($sj_spic_value)):?>
                          <img src="<?php echo pods_field_display('four_seasons', $post->ID, 'four_seasons_spic');?>" alt="<?php the_title(); ?> " title="<?php the_title(); ?> "/>
                       <?php else: ?>
                         <img src="<?php echo pods_field_display('four_seasons', $post->ID, 'four_seasons_big_pic');?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?> "/>
                       <?php endif; ?>
                     </div>
                    <div class="shjy_one_txt"><?php the_title(); ?></div>
                 </div>
              </a>  
              <div class="cd-popup cd-popup<?php the_ID(); ?>">
                <div class="cd-popup-container">
                    <div class="shjy_box_big">
                        <img src="<?php echo pods_field_display('four_seasons', $post->ID, 'four_seasons_big_pic');?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?> "/>
                    </div>
                    <a href="#0" class="shjy_box_close cd-popup-close<?php the_ID(); ?>"></a>
                </div>
            </div>
          </div>  
  <?php endwhile;?>
<?php  weblizar_pagination();?>
</div>
 <script type="text/javascript">
  /*弹框JS内容*/
  jQuery(document).ready(function($){
    <?php while (have_posts()) : the_post();  ?>
      $('.cd-popup-trigger<?php the_ID();?>').on('click', function(event){
            //console.log("lalalal:",<?php echo $key;?>);
            event.preventDefault();
            $('.cd-popup<?php the_ID();?>').addClass('is-visible');
            console.log("addClass",".cd-popup<?php echo the_ID();?>");
            //$(".dialog-addquxiao").hide()
          });
          //关闭窗口
          $('.cd-popup<?php the_ID();?>').on('click', function(event){
              if( $(event.target).is('.cd-popup-close<?php the_ID();?>') || $(event.target).is('.cd-popup<?php echo the_ID();?>') ) {
                  event.preventDefault();
                  $(this).removeClass('is-visible');
              }
          });
          //ESC关闭
          $(document).keyup(function(event){
              if(event.which=='27'){
                  $('.cd-popup<?php the_ID();?>').removeClass('is-visible');
              }
          });
   <?php endwhile;?>
 });
</script>
<?php endif;?>


<?php if(is_category(39)):       //学习心得?>
<div class="neiye_hdyg">
  <div class="neiye_title"><?php $thiscat = get_category($cat); echo $thiscat ->name;?></div>
  <ul class="neiye_list_ul">
      <?php while (have_posts()) : the_post(); ?>      
        <li>
          <label>·</label>
          <span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><label><?php the_time('Y-m-d'); ?></label></span>
        </li>
      <?php endwhile;?>
  </ul>
  <?php  weblizar_pagination();?>
</div>
<?php endif;?>

<?php if(is_category(41)):     //书籍下载?>
<div class="neiye_xiazai neiye_shuji">
   <div class="neiye_xiazai_title">
      <?php 
        $categories = get_categories(array('parent' => 40,'hide_empty' => 0,'orderby'=>term_id,'order'=>asc));
        foreach ($categories as $key => $value) {
          if($cat == $value->term_id){
            echo "<a class='on' href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }else{
            echo "<a href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }
        }  
      ?>
  </div>
<?php while (have_posts()) : the_post(); ?>      
  <div class="neiye_xiazai_one col-md-3 col-sm-3 col-xs-6">
     <div class="neiye_xiazai_box neiye_shuji_box">
         <?php the_post_thumbnail();?>
         <label><?php the_title();?></label>
         <a href="<?php the_permalink();?>" target="_blank">點擊查看</a>
     </div>
  </div>
<?php endwhile;?>
</div>
<?php  weblizar_pagination();?>
<?php endif;?>


<?php if(is_category(42)):    //音乐下载?>
<div class="neiye_xiazai neiye_yinyue">
   <div class="neiye_xiazai_title">
      <?php 
        $categories = get_categories(array('parent' => 40,'hide_empty' => 0,'orderby'=>term_id,'order'=>asc));
        foreach ($categories as $key => $value) {
          if($cat == $value->term_id){
            echo "<a class='on' href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }else{
            echo "<a href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }
        }  
      ?>
  </div>
<?php while (have_posts()) : the_post(); ?>      
  <div class="neiye_xiazai_one col-md-3 col-sm-3 col-xs-6">
     <div class="neiye_xiazai_box neiye_yinyue_box">
         <div class="neiye_yinyue_img">
              <img src="<?php echo pods_field_display('file_download', $post->ID, 'file_download_pic')?>"/>
              <div class="neiye_yinyue_icon"><img src="<?php bloginfo('template_url');?>/images/m_icon.png"/></div>
         </div>
         <label><?php the_title();?></label>
         <a target="_blank" href="<?php echo pods_field_display('file_download', $post->ID, 'file_download_link')?>">跳转网盘下載</a>
     </div>
  </div>
<?php endwhile;?>
</div>
  <?php  weblizar_pagination();?>
<?php endif;?>


<?php if(is_category(43)):   //视频下载?>
<div class="neiye_xiazai neiye_shipin">
   <div class="neiye_xiazai_title">
      <?php 
        $categories = get_categories(array('parent' => 40,'hide_empty' => 0,'orderby'=>term_id,'order'=>asc));
        foreach ($categories as $key => $value) {
          if($cat == $value->term_id){
            echo "<a class='on' href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }else{
            echo "<a href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }
        }  
      ?>
  </div>
<?php while (have_posts()) : the_post(); ?>      
  <div class="neiye_xiazai_one col-md-4 col-sm-4 col-xs-6">
     <div class="neiye_xiazai_box neiye_shipin_box">
         <div class="neiye_shipin_img">
              <img src="<?php echo pods_field_display('file_download', $post->ID, 'file_download_pic')?>"/>
              <div class="neiye_shipin_icon"><img src="<?php bloginfo('template_url');?>/images/v_icon.png"/></div>
         </div>
         <label><?php the_title();?></label>
         <a target="_blank" href="<?php echo pods_field_display('file_download', $post->ID, 'file_download_link')?>">跳转网盘下載</a>
     </div>
  </div>
<?php endwhile;?>
</div>
<?php  weblizar_pagination();?>
<?php endif;?>



<?php 
    // $defaults = array(
    //         'before'           => '<div id="pagination"><ul class="page-numbers">' . __( 'Pages:'),
    //         'after'            => '</ul></div>',
    //         'link_before'      => true,
    //         'link_after'       => true,
    //         'next_or_number'   => 'number',
    //         'separator'        => ' ',
    //         'nextpagelink'     => __( 'Next page'),
    //         'previouspagelink' => __( 'Previous page'),
    //         'pagelink'         => '%',
    //         'echo'             => 1
    //         );
    // wp_link_pages( $defaults );
?>

