<?php get_header(); ?>
<div class="enigma_header_breadcrum_title">	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>404错误</h1>
				<ul class="breadcrumb">
					<li><a href="<?php echo home_url( '/' ); ?>">首页</a></li>
					<li>404 错误</li>
				
				</ul>
			</div>
		</div>
	</div>	
</div>
<div class="container">
	<div class="row enigma_blog_wrapper">
		<div class="col-md-12 hc_404_error_section">
			<div class="error_404">
				<h2>404</h2>
				<h4>您要查看的页面飞走啦o(╯□╰)o</h4>
				<p>很抱歉，您要查看的页面不存在</p>
				<p><a href="<?php echo home_url( '/' ); ?>"><button class="enigma_send_button" type="submit">前往首页</button></a></p>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>