<?php
/** Theme Name	: Enigma
* Theme Core Functions and Codes
*/
	/**Includes required resources here**/
	define('WL_TEMPLATE_DIR_URI', get_template_directory_uri());
	define('WL_TEMPLATE_DIR', get_template_directory());
	define('WL_TEMPLATE_DIR_CORE' , WL_TEMPLATE_DIR . '/core');
	require( WL_TEMPLATE_DIR_CORE . '/menu/default_menu_walker.php' );
	require( WL_TEMPLATE_DIR_CORE . '/menu/weblizar_nav_walker.php' );
	require( WL_TEMPLATE_DIR_CORE . '/scripts/css_js.php' ); //Enquiring Resources here	
	require( WL_TEMPLATE_DIR_CORE . '/comment-function.php' );	
	require(dirname(__FILE__).'/customizer.php');
	
	//Sane Defaults
	function weblizar_default_settings()
{
	$ImageUrl =  esc_url(get_template_directory_uri() ."/images/1.png");
	$ImageUrl2 = esc_url(get_template_directory_uri() ."/images/2.png");
	$ImageUrl3 = esc_url(get_template_directory_uri() ."/images/3.png");
	$ImageUrl4 = esc_url(get_template_directory_uri() ."/images/portfolio1.png");
	$ImageUrl5 = esc_url(get_template_directory_uri() ."/images/portfolio2.png");
	$ImageUrl6 = esc_url(get_template_directory_uri() ."/images/portfolio3.png");
	$ImageUrl7 = esc_url(get_template_directory_uri() ."/images/portfolio4.png");
	$wl_theme_options=array(
			//Logo and Fevicon header			
			'upload_image_logo'=>'',
			'height'=>'55',
			'width'=>'150',
			'_frontpage' => '1',
			'blog_count'=>'3',
			'upload_image_favicon'=>'',			
			'custom_css'=>'',
			'slide_image_1' => $ImageUrl,
			'slide_title_1' => __('Slide Title', 'enigma' ),
			'slide_desc_1' => __('Lorem Ipsum is simply dummy text of the printing', 'enigma' ),
			'slide_btn_text_1' => __('Read More', 'enigma' ),
			'slide_btn_link_1' => '#',
			'slide_image_2' => $ImageUrl2,
			'slide_title_2' => __('variations of passages', 'enigma' ),
			'slide_desc_2' => __('Contrary to popular belief, Lorem Ipsum is not simply random text', 'enigma' ),
			'slide_btn_text_2' => __('Read More', 'enigma' ),
			'slide_btn_link_2' => '#',
			'slide_image_3' => $ImageUrl3,
			'slide_title_3' => __('Contrary to popular ', 'enigma' ),
			'slide_desc_3' => __('Aldus PageMaker including versions of Lorem Ipsum, rutrum turpi', 'enigma' ),
			'slide_btn_text_3' => __('Read More', 'enigma' ),
			'slide_btn_link_3' => '#',			

			'footer_copyright' => '',
			'footer_copyright_owner' => '',
			'footer_address' => '',
			'footer_bank_account' => '',
			'footer_account_name' => '',
			'footer_tel_number' => '',
			'footer_email' => '',
			// Footer Call-Out
			// 'fc_home'=>'1',			
			// 'fc_title' => __('Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', 'enigma' ),
			// 'fc_btn_txt' => __('More Features', 'enigma' ),
			// 'fc_btn_link' =>"#",
			// 'fc_icon' => 'fa fa-thumbs-up', 
			//Social media links
			// 'header_social_media_in_enabled'=>'1',
			// 'footer_section_social_media_enbled'=>'1',
			// 'twitter_link' =>"#",
			// 'fb_link' =>"#",
			// 'linkedin_link' =>"#",
			// 'youtube_link' =>"#",
			// 'instagram' =>"#",
			// 'gplus' =>"#",
			
			// 'email_id' => 'example@mymail.com',
			// 'phone_no' => '0159753586',
			// 'footer_customizations' => __(' &#169; 2016 Enigma Theme', 'enigma' ),
			// 'developed_by_text' => __('Theme Developed By', 'enigma' ),
			// 'developed_by_weblizar_text' => __('Weblizar Themes', 'enigma' ),
			// 'developed_by_link' => 'http://weblizar.com/',
			// 'service_home'=>'1',
			// 'home_service_heading' => __('Our Services', 'enigma' ),
			// 'service_1_title'=>__("Idea",'enigma' ),
			// 'service_1_icons'=>"fa fa-google",
			// 'service_1_text'=>__("There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in.", 'enigma' ),
			// 'service_1_link'=>"#",
			
			// 'service_2_title'=>__('Records', 'enigma' ),
			// 'service_2_icons'=>"fa fa-database",
			// 'service_2_text'=>__("There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in.", 'enigma' ),
			// 'service_2_link'=>"#",
			
			// 'service_3_title'=>__("WordPress", 'enigma' ),
			// 'service_3_icons'=>"fa fa-wordpress",
			// 'service_3_text'=>__("There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in.", 'enigma' ),
			// 'service_3_link'=>"#",			

			//Portfolio Settings:
			// 'portfolio_home'=>'1',
			// 'port_heading' => __('Recent Works', 'enigma' ),
			// 'port_1_img'=> $ImageUrl4,
			// 'port_1_title'=>__('Bonorum', 'enigma' ),
			// 'port_1_link'=>'#',
			// 'port_2_img'=> $ImageUrl5,			
			// 'port_2_title'=>__('Content', 'enigma' ),
			// 'port_2_link'=>'#',
			// 'port_3_img'=> $ImageUrl6,
			// 'port_3_title'=>__('dictionary', 'enigma' ),
			// 'port_3_link'=>'#',
			// 'port_4_img'=> $ImageUrl7,
			// 'port_4_title'=>__('randomised', 'enigma' ),
			// 'port_4_link'=>'#',
			//BLOG Settings
			// 'show_blog' => '1',
			// 'blog_title'=>__('Latest Blog', 'enigma' ),
			
			//Google font style
			//'main_heading_font' => 'Open Sans',
			//'menu_font' => 'Open Sans',
			//'theme_title' => 'Open Sans',
			//'desc_font_all' => 'Open Sans'
			
			
		);
		return apply_filters( 'enigma_options', $wl_theme_options );
}
	function weblizar_get_options() {
    // Options API
    return wp_parse_args( 
        get_option( 'enigma_options', array() ), 
        weblizar_default_settings() 
    );    
	}
	
	/*After Theme Setup*/
	add_action( 'after_setup_theme', 'weblizar_head_setup' ); 	
	function weblizar_head_setup()
	{	
		global $content_width;
		//content width
		if ( ! isset( $content_width ) ) $content_width = 550; //px
	
	    //Blog Thumb Image Sizes
		add_image_size('home_post_thumb',340,210,true);
		//Blogs thumbs
		add_image_size('wl_page_thumb',730,350,true);	
		add_image_size('blog_2c_thumb',570,350,true);
		add_theme_support( 'title-tag' );
		// Load text domain for translation-ready
		load_theme_textdomain( 'enigma', WL_TEMPLATE_DIR_CORE . '/lang' );	
		
		add_theme_support( 'post-thumbnails' ); //supports featured image
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', __( 'Primary Menu', 'enigma' ) );
		// theme support 	
		$args = array('default-color' => '000000',);
		add_theme_support( 'custom-background', $args); 
		add_theme_support( 'automatic-feed-links');
		
		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style('css/editor-style.css');
		require( WL_TEMPLATE_DIR . '/options-reset.php'); //Reset Theme Options Here				
	}
	

	// Read more tag to formatting in blog page 
	function weblizar_content_more($more)
	{  							
	   return '<div class="blog-post-details-item"><a class="enigma_blog_read_btn" href="'.get_permalink().'"><i class="fa fa-plus-circle"></i>"'.__('Read More', 'enigma' ).'"</a></div>';
	}   
	add_filter( 'the_content_more_link', 'weblizar_content_more' );
	
	
	// Replaces the excerpt "more" text by a link
	function weblizar_excerpt_more($more) {      
	return '';
	}
	add_filter('excerpt_more', 'weblizar_excerpt_more');
	/*
	* Weblizar widget area
	*/
	add_action( 'widgets_init', 'weblizar_widgets_init');
	function weblizar_widgets_init() {
	/*sidebar*/
	register_sidebar( array(
			'name' => __( 'Sidebar', 'enigma' ),
			'id' => 'sidebar-primary',
			'description' => __( 'The primary widget area', 'enigma' ),
			'before_widget' => '<div class="enigma_sidebar_widget">',
			'after_widget' => '</div>',
			'before_title' => '<div class="enigma_sidebar_widget_title"><h2>',
			'after_title' => '</h2></div>'
		) );

	register_sidebar( array(
			'name' => __( 'Footer Widget Area', 'enigma' ),
			'id' => 'footer-widget-area',
			'description' => __( 'footer widget area', 'enigma' ),
			'before_widget' => '<div class="col-md-3 col-sm-6 enigma_footer_widget_column">',
			'after_widget' => '</div>',
			'before_title' => '<div class="enigma_footer_widget_title">',
			'after_title' => '<div class="enigma-footer-separator"></div></div>',
		) );             
	}
	
	/* Breadcrumbs  */
	function weblizar_breadcrumbs() {
    $delimiter = '';
    $home = __('Home', 'enigma' ); // text for the 'Home' link
    $before = '<li>'; // tag before the current crumb
    $after = '</li>'; // tag after the current crumb
    echo '<ul class="breadcrumb">';
    global $post;
    $homeLink = home_url();
    echo '<li><a href="' . $homeLink . '">' . $home . '</a></li>' . $delimiter . ' ';
    if (is_category()) {
        global $wp_query;
        $cat_obj = $wp_query->get_queried_object();
        $thisCat = $cat_obj->term_id;
        $thisCat = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0)
            echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
        echo $before . ' _e("Archive by category","enigma") "' . single_cat_title('', false) . '"' . $after;
    } elseif (is_day()) {
        echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
        echo '<li><a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
        echo $before . get_the_time('d') . $after;
    } elseif (is_month()) {
        echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
        echo $before . get_the_time('F') . $after;
    } elseif (is_year()) {
        echo $before . get_the_time('Y') . $after;
    } elseif (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            echo '<li><a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a></li> ' . $delimiter . ' ';
            echo $before . get_the_title() . $after;
        } else {
            $cat = get_the_category();
            $cat = $cat[0];
            //echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            echo $before . get_the_title() . $after;
        }
		
    } elseif (!is_single() && !is_page() && get_post_type() != 'post') {
        $post_type = get_post_type_object(get_post_type());
        echo $before . $post_type->labels->singular_name . $after;
    } elseif (is_attachment()) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID);
        $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo '<li><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
    } elseif (is_page() && !$post->post_parent) {
        echo $before . get_the_title() . $after;
    } elseif (is_page() && $post->post_parent) {
        $parent_id = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
            $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb)
            echo $crumb . ' ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
    } elseif (is_search()) {
        echo $before . _e("Search results for","enigma")  . get_search_query() . '"' . $after;

    } elseif (is_tag()) {        
		echo $before . _e('Tag','enigma') . single_tag_title('', false) . $after;
    } elseif (is_author()) {
        global $author;
        $userdata = get_userdata($author);
        echo $before . _e("Articles posted by","enigma") . $userdata->display_name . $after;
    } elseif (is_404()) {
        echo $before . _e("Error 404","enigma") . $after;
    }
    
    echo '</ul>';
	}
	
	
	//PAGINATION
		function weblizar_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
     	echo "<div id='pagination'><span class='pages'>共".$pages."頁</span><ul class='page-numbers'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a class='page-numbers' href='".get_pagenum_link(1)."'>&laquo;</a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li><a class='page-numbers' href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
             	echo ($paged == $i)? "<li><span class='page-numbers current'>".$i."</span></li>":"<li><a class='page-numbers' href='".get_pagenum_link($i)."'>".$i."</a></li>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<li><a class='page-numbers' href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a></li>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a class='page-numbers' href='".get_pagenum_link($pages)."'>&raquo;</a></li>";
         echo "</ul></div>";
     }
}
	/*===================================================================================
	* Add Author Links
	* =================================================================================*/
	function weblizar_author_profile( $contactmethods ) {	
	
	$contactmethods['youtube_profile'] = __('Youtube Profile URL','enigma');	
	$contactmethods['twitter_profile'] = __('Twitter Profile URL','enigma');
	$contactmethods['facebook_profile'] = __('Facebook Profile URL','enigma');
	$contactmethods['linkedin_profile'] = __('Linkedin Profile URL','enigma');
	
	return $contactmethods;
	}
	add_filter( 'user_contactmethods', 'weblizar_author_profile', 10, 1);
	/*===================================================================================
	* Add Class Gravtar
	* =================================================================================*/
	add_filter('get_avatar','weblizar_gravatar_class');

	function weblizar_gravatar_class($class) {
    $class = str_replace("class='avatar", "class='author_detail_img", $class);
    return $class;
	}	
	/****--- Navigation for Author, Category , Tag , Archive ---***/
	function weblizar_navigation() { ?>
	<div class="enigma_blog_pagination">
	<div class="enigma_blog_pagi">
	<?php posts_nav_link(); ?>
	</div>
	</div>
	<?php }

	/****--- Navigation for Single ---***/
	function weblizar_navigation_posts() { ?>
	<div class="navigation_en">
	<nav id="wblizar_nav"> 
	<span class="nav-previous">
	<?php previous_post_link('&laquo; %link'); ?>
	</span>
	<span class="nav-next">
	<?php next_post_link('%link &raquo;'); ?>
	</span> 
	</nav>
	</div>	
<?php 
	}
if (is_admin()) {
	require_once('core/admin/admin-themes.php');
}	

/**
 * 注册新菜单
 */
register_nav_menus( array(
	'primary' => 'Menu Left', //这里的primary 你可以自己设置，你也可以写成top-menu ；但是要在调用菜单时也用同样的名字。
	'secondary' => 'Menu Person1',
	'liangjia' => 'Menu Person2'
));

/**
 * WordPress 后台禁用Google Open Sans字体，加速网站
 * http://www.wpdaxue.com/disable-google-fonts.html
 */
add_filter( 'gettext_with_context', 'wpdx_disable_open_sans', 888, 4 );
function wpdx_disable_open_sans( $translations, $text, $context, $domain ) {
  if ( 'Open Sans font: on or off' == $context && 'on' == $text ) {
    $translations = 'off';
  }
  return $translations;
}

//移除无用wp_head包含项
// Actions
//remove_action( 'wp_head' , 'wp_enqueue_scripts', 1 );
remove_action( 'wp_head' , 'feed_links', 2 );    //文章和评论feed
remove_action( 'wp_head' , 'feed_links_extra', 3 );    //分类等feed
remove_action( 'wp_head' , 'rsd_link' );       //移除离线编辑器开放接口
remove_action( 'wp_head' , 'wlwmanifest_link' );
remove_action( 'wp_head' , 'index_rel_link' );      //移除前后文，第一篇文章，主页meta信息
remove_action( 'wp_head' , 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head' , 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head' , 'adjacent_posts_rel_link_wp_head', 10, 0 );
//remove_action( 'wp_head' , 'locale_stylesheet' );
//remove_action( 'wp_head' , 'noindex', 1 );
//remove_action( 'wp_head' , 'wp_print_styles', 8 );
//remove_action( 'wp_head' , 'wp_print_head_scripts', 9 );
remove_action( 'wp_head' , 'wp_generator' );           //移除Wordpress版本
//remove_action( 'wp_head' , 'rel_canonical' );
//remove_action( 'wp_head' , 'wp_shortlink_wp_head', 10, 0 );


/**
 * 为WordPress页面添加标签和分类
 */
class PTCFP{
	function __construct(){
	add_action( 'init', array( $this, 'taxonomies_for_pages' ) );
	/**
	 * 确保这些查询修改不会作用于管理后台，防止文章和页面混杂 
	 */
	if ( ! is_admin() ) {
	  add_action( 'pre_get_posts', array( $this, 'category_archives' ) );
	  add_action( 'pre_get_posts', array( $this, 'tags_archives' ) );
	} // ! is_admin
	} // __construct
	/**
	* 为“页面”添加“标签”和“分类”
	*
	* @uses register_taxonomy_for_object_type
	*/
	function taxonomies_for_pages() {
	  register_taxonomy_for_object_type( 'post_tag', 'page' );
	  register_taxonomy_for_object_type( 'category', 'page' );
	} // taxonomies_for_pages

	/**
	* 在标签存档中包含“页面”
	*/
    function tags_archives( $wp_query ) {
		if ( $wp_query->get( 'tag' ) )
		  $wp_query->set( 'post_type', 'any' );

		} // tags_archives
	/**
	* 在分类存档中包含“页面”
	*/
	function category_archives( $wp_query ) {
	if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) )
	  $wp_query->set( 'post_type', 'any' );

	} // category_archives
} // PTCFP
$ptcfp = new PTCFP();


//增强编辑器
function add_editor_buttons($buttons) {
	$buttons[] = 'fontselect';
	$buttons[] = 'fontsizeselect';
	$buttons[] = 'cleanup';
	$buttons[] = 'styleselect';
	$buttons[] = 'hr';
	$buttons[] = 'del';
	$buttons[] = 'sub';
	$buttons[] = 'sup';
	$buttons[] = 'copy';
	$buttons[] = 'paste';
	$buttons[] = 'cut';
	$buttons[] = 'undo';
	$buttons[] = 'image';
	$buttons[] = 'anchor';
	$buttons[] = 'backcolor';
	$buttons[] = 'wp_page';
	$buttons[] = 'charmap';
	return $buttons;
}
add_filter("mce_buttons_3", "add_editor_buttons");

//找回上传设置
if(get_option('upload_path')=='content/uploads' || get_option('upload_path')==null) {
	update_option('upload_path',WP_CONTENT_DIR.'/uploads');
}

/**
*实现文章中若有特色图片，取特色图片，若无，则取文章中第一张图片
*/
function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  if(has_post_thumbnail()) {
        echo '<a href="'.get_permalink().'">';
        the_post_thumbnail();
        echo '</a>';
  }else{
     $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
     $first_img = $matches [1] [0];
     
     if(empty($first_img)){  
     	echo '<a href="'.get_permalink().'"><img src="'.get_stylesheet_directory_uri().'/images/activity4.jpg" /></a>'; 
     }else{
     	echo '<a href="'.get_permalink().'"><img src="'.$first_img.'" /></a>';
     }
  }
}

/**
 * 返回根分类的id号
 * @param  [type] $cat [description]
 * @return [type]      [description]
 */
function get_root_id($cat)
{
	if(is_category()){
		$this_category = get_category($cat); // 取得当前分类
	}else{
		$this_category = get_the_category()[0];
	}
	
	while($this_category->category_parent) // 若当前分类有上级分类时，循环
	{
		$this_category = get_category($this_category->category_parent); // 将当前分类设为上级分类（往上爬）
	}
	return $this_category->term_id; // 返回根分类的id号
}

/**
 * 允许.mobi文件上传至媒体库
 * @param [type] $mimes [description]
 */
function add_mime_types( $mimes ){
	$mimes['mobi'] = 'text/mobi';
	return $mimes;
}
add_filter( 'upload_mimes', 'add_mime_types' );

//官方Gravatar头像调用ssl头像链接
function get_ssl_avatar($avatar) {
 $avatar = preg_replace('/.*\/avatar\/(.*)\?s=([\d]+)&.*/','<img src="https://secure.gravatar.com/avatar/$1?s=$2" class="avatar avatar-$2" height="$2" width="$2">',$avatar);
 return $avatar;
}
add_filter('get_avatar', 'get_ssl_avatar');


//判断是电脑端访问还是手机端访问,若是手机端返回true，若是PC，返回true
function is_mobile_request()
{
    $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP'])?$_SERVER['ALL_HTTP'] : '';
    $mobile_browser = '0';
    if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i',
    strtolower($_SERVER['HTTP_USER_AGENT'])))
        $mobile_browser++;
    if((isset($_SERVER['HTTP_ACCEPT'])) and
    (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))
        $mobile_browser++;
    if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
        $mobile_browser++;
    if(isset($_SERVER['HTTP_PROFILE']))
        $mobile_browser++;
    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
    $mobile_agents = array(
    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
    'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
    'wapr','webc','winw','winw','xda','xda-'
    );
    if(in_array($mobile_ua, $mobile_agents))
        $mobile_browser++;
    if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)
        $mobile_browser++;
    // Pre-final check to reset everything if the user is on Windows
    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false)
        $mobile_browser=0;
    // But WP7 is also Windows, with a slightly different characteristic
    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false)
        $mobile_browser++;
    if($mobile_browser>0)
        return true;
    else
        return false;
}

//为wordpress编辑器加上中文字体
function custum_fontfamily($initArray){
   $initArray['font_formats'] = "微软雅黑='微软雅黑';宋体='宋体';黑体='黑体';仿宋='仿宋';楷体='楷体';隶书='隶书';幼圆='幼圆;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats";
   return $initArray;
}
add_filter('tiny_mce_before_init', 'custum_fontfamily');

// 同时删除head和feed中的WP版本号
function remove_wp_version() {
  return '';
}
add_filter('the_generator', 'remove_wp_version');

// 隐藏js/css附加的WP版本号
function remove_wp_version_strings( $src ) {
  global $wp_version;
  parse_str(parse_url($src, PHP_URL_QUERY), $query);
  if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
    // 用WP版本号 + 22.2来替代js/css附加的版本号
    // 既隐藏了WordPress版本号，也不会影响缓存
    // 建议把下面的 22.2 替换成其他数字，以免被别人猜出
    $src = str_replace($wp_version, $wp_version + 22.2, $src);
  }
  return $src;
}
add_filter( 'script_loader_src', 'remove_wp_version_strings' );
add_filter( 'style_loader_src', 'remove_wp_version_strings' );


//修改后台显示更新的代码
// add_filter('pre_site_transient_update_core',    create_function('$a', "return null;")); // 关闭核心提示
// add_filter('pre_site_transient_update_plugins', create_function('$a', "return null;")); // 关闭插件提示
add_filter('pre_site_transient_update_themes',  create_function('$a', "return null;")); // 关闭主题提示
// remove_action('admin_init', '_maybe_update_plugins'); // 禁止 WordPress 更新插件
// remove_action('admin_init', '_maybe_update_core');    // 禁止 WordPress 检查更新
remove_action('admin_init', '_maybe_update_themes');  // 禁止 WordPress 更新主题


/* Add this code to functions.php file in your theme */
register_sidebar(array(
	'name' => 'Footer Widget 1',
	'id'        => 'footer-1',
	'description' => 'First footer widget area',
	'before_widget' => '<div class="col-md-2 col-sm-2 col-xs-2"><dl>',
	'after_widget' => '</dl></div>',
	'before_title' => '<dt>',
	'after_title' => '</dt>',
));
?>