<?php
	 /*
	 Template Name: 网上预约模板
	 */
?>
<?php get_header();  ?>
<div class="clearfix neiye_banner">
		<div class="row">
		<?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			  the_post_thumbnail();
			}else{   ?>
			<img src="<?php bloginfo('template_url');?>/images/ziguitiigui.jpg"/>
		<?php }?>
	</div>
</div>
<div class="clearfix">
  <div class="container">
    <div class="row">
		<div class="row neiye_warpper">
		    <?php get_sidebar(); ?>	
			<div class="col-md-9 col-sm-9 col-xs-12">
			  <div class="booking_content">
				<h1><?php the_field('page_big_title');?></h1>
				<div class="booking_notes"><?php the_field('booking_notice');?></div>
				<div class="booking_time">
					<div class="booking_label_head">
					    <label>○</label><?php the_field('booking_time_title');?>
					</div>
					<div class="booking_time_table">
						<?php the_field('booking_time_content');?>
					</div>
				</div>
				<div class="booking_statute">
					<div class="booking_label_head">
						<label>○</label><?php the_field('booking_statute_title');?>
					</div>
					<div class="booking_statute_content">
						<div class="booking_statute_ctop">
							<img src="<?php bloginfo('template_url');?>/images/booking_line.jpg"/>
						</div>
						<div class="booking_statute_txt">
							<?php the_field('booking_statue_content');?>
					    </div>
					    <div class="booking_statute_cbelow">
							<img src="<?php bloginfo('template_url');?>/images/booking_line.jpg"/>
						</div>
					</div>
				</div>
			</div>
		  </div>
		</div>   
	</div>
   </div>	
   <div class="booking_apply_bg">
	   <div class="container">
			  <div class="booking_apply">
			  	<span><?php the_field('booking_stxt');?><input type="checkbox" name="booking_apply_read"/></span>
			  	<input type="button" disabled="true" name="booking_apply_live" value="<?php the_field('booking_btn_txt');?>" />
			  </div>
		      <div class="cd-popup apply-form">
				    <div class="cd-popup-container">
				    	 <h2>生活营申请表</h2>
					     <?php wd_form_maker(1); ?>
					     <a href="#0" class="scene_box_close cd-popup-close"></a>
				    </div>
			  </div>
		</div>
	</div>
</div>
<?php get_footer(); ?>


 <script type="text/javascript">
	
	jQuery(document).ready(function($){
		    /*弹框JS内容开始*/
			$("input[name='booking_apply_live']").on('click', function(event){
	            //console.log("lalalal:","执行了弹出事件");
	            event.preventDefault();
	            $('.cd-popup').addClass('is-visible');
	            //console.log("addClass",".cd-popup");
	            //$(".dialog-addquxiao").hide()
	        });
	        //关闭窗口
	        $('.cd-popup').on('click', function(event){
	            if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
	                event.preventDefault();
	                $(this).removeClass('is-visible');
	            }
	        });
	        //ESC关闭
	        $(document).keyup(function(event){
	            if(event.which=='27'){
	                $('.cd-popup').removeClass('is-visible');
	            }
	        });
	        /*弹框JS内容结束*/

	        $("input[name='booking_apply_read']").on('click', function(event){
	        	if($(this).prop("checked")) {
	        		$("input[name='booking_apply_live']").removeAttr("disabled");
	        		$("input[name='booking_apply_live']").css("background","#99594c");
	        	}else{
	        		$("input[name='booking_apply_live']").attr("disabled","disabled");
	        		$("input[name='booking_apply_live']").css("background","#aaa");
	        	}
	        });


	        jQuery(".wdform-label-section").css({
	        	"width":"100%",
	        	"float":"left",
	        });
	        jQuery(".wdform-field").css({
				"width":'100%',
				"display":"block",
				"height":"auto",
			});
			jQuery(".input_deactive").css({
				"width":"100%",
				"border":"1px solid #ddd",
				"background":"#fff",
			});
	        jQuery(".wdform-ch-rad-label").css({
				"color":"#555",
				"font-weight":"normal",
				"font-size":"1.5rem",
				"line-height":"10px",
			});
			jQuery(".wdform-label").css({
				"color":"#555",
				"font-size":"1.4rem",
				"line-height":"20px",
			});
			jQuery(".wdform-element-section").css({
				"width":"100%",
				"display":"block",
			});
			jQuery(".wdform-element-section  textarea").css({
				"height":"60px",
			});
			jQuery(".wdform-page-and-images").css({
				"padding":"0px",
			});
			jQuery(".wdform_select select").css({
				"background":"#fff",
			});
			jQuery("#wdform_13_element1").css({
				"background":"#fff",
			});
			jQuery(".button-submit").css({
				"background":"#99594c",
				"margin-left":"42%",
			});
	 });

	</script>