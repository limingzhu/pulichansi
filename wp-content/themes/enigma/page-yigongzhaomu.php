<?php
	 /*
	 Template Name: 义工招募模板
	 */
?>
<?php get_header(); ?>
<div class="clearfix neiye_banner">
		<div class="row">
		<?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			  the_post_thumbnail();
			}
		?>
	</div>
</div>
<div class="clearfix">
  <div class="container">
    <div class="row">
		<div class="row neiye_warpper ">
		    <?php get_sidebar(); ?>	
			<div class="col-md-9 col-sm-9 col-xs-12">
			  <div class="neiye_content neiye_yigong">
				   <?php 
		                while ( have_posts() ) : the_post();
		                   the_content();
		                endwhile;
	                ?>
	                <h1><?php the_field('page_big_title');?></h1>
	                <div class="neiye_yigong_form">
		                <?php wd_form_maker(10); ?>
		            </div>
              </div>
			</div>
		</div>   
	</div>
   </div>	
</div>
<?php get_footer(); ?>

<script type="text/javascript">
	jQuery(function(){
		//console.log(jQuery(".neiye_warpper p"));
		jQuery(".wdform_section .wdform_column:last-child ").css("padding","0px");
		
		jQuery(".neiye_content p").each(function(){
			if(jQuery(this).has('img').length){
				jQuery(this).css("text-indent",0);
			}
		});

		jQuery(".wdform-field").css({
			"width":'100%',
			"display":"block",
			"height":"auto",
		});
		jQuery(".wdform-label-section").css("width","100%");
		jQuery(".wdform-element-section").css("width","100%");
		jQuery(".input_deactive").css({
			"width":"100%",
			"height":"30px",
			"border":"1px solid #ddd",
			"background":"#fff",
		});
		jQuery(".input_active").css({
			"border":"1px solid #99594c"
		});
		jQuery(".wdform-element-section select").css({
			"width":"100%",
			"height":"30px",
			"border":"1px solid #ddd",
			"background":"#fff",
		});
		jQuery(".wdform-label").css({
			"color":"#555",
			"font-size":"1.5rem",
			"line-height":"30px",
		});
		jQuery(".wdform-required").css({
			"line-height":"30px",
		});
		jQuery("#wdform_19_element10").css({
			"height":"150px",
		});
		jQuery(".wdform-ch-rad-label").css({
			"color":"#555",
			"font-weight":"normal",
			"font-size":"1.5rem",
			"line-height":"30px",
		});
		jQuery(".radio-div").css({
			"line-height":"25px",
		});
		jQuery(".wdform_date_fields div:first").css({
			"width":"100%",
		});
		jQuery(".wdform-page-and-images").css({
			"padding":"0px",
		});
	})
</script>



