<?php
	 /*
	 Template Name: 首页模板
	 */
?>
<?php 
    get_header(); 
	get_template_part('home','slideshow'); 
?>

<!--參觀來訪-->
<div class="clearfix">
	<div class="visit_section">
		<div class="container">
			<div class="row ">
				<!--<div class="col-md-12 col-sm-12">
					<h2>參觀來訪</h2>
				</div>-->
				<div class="col-md-4 col-sm-4 col-xm-12">
					<div class="visit_block">
					  <img src="<?php if(get_field('canguanlaifang_img1')!=''){ the_field('canguanlaifang_img1');}else{echo bloginfo('template_url')."/images/visit1.jpg";} ?>"/>
					  <a href="<?php if(get_field('canguanlaifang_link1')!=''){ the_field('canguanlaifang_link1');}else{echo "/volunteer/";} ?> ">
						  <div class="visit_txt">
							  <?php if(get_field('canguanlaifang_text1')!=''){ the_field('canguanlaifang_text1');}else{echo "義工報名";} ?> 	
						  </div>
					  </a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xm-12">
					<div class="visit_block">
					  <img src="<?php if(get_field('canguanlaifang_img2')!=''){ the_field('canguanlaifang_img2');}else{echo bloginfo('template_url')."/images/visit2.jpg";} ?>"/>
					  <a href="<?php if(get_field('canguanlaifang_link2')!=''){ the_field('canguanlaifang_link2');}else{echo "/booking/";} ?>">
						  <div class="visit_txt">
							  <?php if(get_field('canguanlaifang_text2')!=''){ the_field('canguanlaifang_text2');}else{echo "申請共住";} ?> 	
						  </div>
					  </a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xm-12">
					<div class="visit_block">
					  <img src="<?php if(get_field('canguanlaifang_img3')!=''){ the_field('canguanlaifang_img3');}else{echo bloginfo('template_url')."/images/visit3.jpg";} ?>"/>
					  <a href="<?php if(get_field('canguanlaifang_link3')!=''){ the_field('canguanlaifang_link3');}else{echo "/traffic/";} ?>">
					     <div class="visit_txt">
						     <?php if(get_field('canguanlaifang_text3')!=''){ the_field('canguanlaifang_text3');}else{echo "交通攻略";} ?> 
					     </div>
					  </a>
					</div>
				</div>
		    </div>
		</div>
	</div>
</div>


<!--文化活动-->
<div class="clearfix cultural_activity_bg">
<img class="cultural_activity_bg_img" src="<?php bloginfo('template_url');?>/images/bg1.jpg"/>
    <div class="cultural_section">
		<div class="container">
			<div class="row ">
				<!--<div class="col-md-12 col-sm-12">
					<h2>文化活動</h2>
				</div>-->
				<div class="col-md-6 col-sm-6 col-xm-12">
					<div class="cultural_activity">
						<div class="cul_header">
							<div class="cul_header_left"><label>○</label><?php if(get_field('huodong_text')!=''){ the_field('huodong_text');}else{echo "成果展示";} ?> </div>
							<a href="/category/duyingwudao/chengguozhanshi/" class="cul_header_more">更多>></a>
						</div>
						<div class="ca_content">
						 <?php
				           query_posts('cat=13&showposts=4&orderby=date&order=DESC');
				         ?>
						 <?php while (have_posts()) : the_post(); ?>
						 	<div class="col-md-6 col-sm-6 col-xs-12 ca_content_img">				 	
							 	<?php catch_that_image();?>
							 	<div class="ca_content_txt"><a href="<?php the_permalink();?>" title="<?php the_title();?>" alt="<?php the_title();?>">【<?php the_title();?>】</a></div>
						 	</div>
				          <?php endwhile;?>
				         <?php wp_reset_query();?>
							<!-- <div class="col-md-6 col-sm-6 col-xs-12 ca_content_img"><a href=""><img src="<?php bloginfo('template_url');?>/images/activity4.jpg"/></a><div class="ca_content_txt"><a href="">【讀書會】</a></div></div> -->
						</div>

					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xm-12">
					<div class="cultual_news">
					    <div class="cul_header">
							<div class="cul_header_left"><label>○</label><?php if(get_field('xinwen_text')!=''){ the_field('xinwen_text');}else{echo "新聞";} ?></div>
							<a href="/category/duyingwudao/huodongyugao/" class="cul_header_more">更多>></a>
						</div>
						<div class="cn_content">
							<!--<div class="cn_content_list"><label><?php echo date("m-d",strtotime(get_page(577)->post_date));?></label><a href="<?php echo get_permalink(577);?>" title="<?php echo get_the_title(577); ?>" alt="<?php echo get_the_title(577); ?>"><strong><?php echo get_the_title(577); ?></strong></a></div>-->
							<div class="cn_content_list"><label><?php echo date("m-d",strtotime(get_page(1055)->post_date));?></label><a href="<?php echo get_permalink(1055);?>" title="<?php echo get_the_title(1055); ?>" alt="<?php echo get_the_title(1055); ?>"><strong><?php echo get_the_title(1055);?></strong></a></div>
							<?php
					           query_posts('cat=12,13&showposts=7&orderby=date&order=DESC');
					         ?>
							 <?php while (have_posts()) : the_post(); ?>
							 	<div class="cn_content_list"><label><?php the_time('m-d'); ?></label><a href="<?php the_permalink();?>" title="<?php the_title();?>" alt="<?php the_title();?>"><?php the_title();?></a></div>
					          <?php endwhile;?>
					         <?php wp_reset_query();?>
						     <!-- <div class="cn_content_list"><label>01-23</label><a href="" title="" alt="">洞山普利禪寺夏令營開始報名啦</a></div> -->
						</div>
					  
					</div>
				</div>
		    </div>
		</div>
	</div>
</div>

<!--佛法珠玑--> 
<div class="clearfix dharma_bg">
    <div class="dharma_section">
		<div class="container">
			<div class="row ">
				<div class="col-md-12 col-sm-12">
					<h2><?php if(get_field('fofazhuji_text')!=''){ the_field('fofazhuji_text');}else{echo "佛法珠璣";} ?></h2>
					<div class="dharma_content">
					 <ul>
					     <?php
				          $query_post = array(
				            'posts_per_page' => 15,
				            'post__in' => get_option('sticky_posts'),
				            'caller_get_posts' => 1,
				            'cat' => 15,
				            );
				            query_posts($query_post);
				          ?>
					     <?php
				           // query_posts('cat=15&showposts=15&orderby=date&order=DESC');
				          ?>
				          <?php while (have_posts()) : the_post(); ?>
				          <li>
				            <label>○</label><a href="<?php the_permalink();?>" alt="<?php the_title();?>" title="<?php the_title();?>"><?php the_title();?></a>
				          </li>
				          <?php endwhile;?>
				          <?php wp_reset_query(); ?>
					 </ul>
					 <div class="dharma_more">
					 	<a href="/category/duyingwudao/fofazhuji/">查看更多</a>
					 </div>
					</div>
				</div>
		    </div>
		</div>
	</div>
</div>


<!--推薦-->
<div class="clearfix recommend_bg">
    <div class="recommend_section">
		<div class="container">
			<div class="row ">
				 <ul class="recommend_tab" role="tablist">
					 <li role="presentation" class="active"><a href="#shuji" aria-controls="shuji" role="tab" data-toggle="tab">/<label>書籍</label>/</a></li>
					 <li role="presentation"><a href="#yinyue" aria-controls="yinyue" role="tab" data-toggle="tab"><label>音樂</label>/</a></li>
					 <li role="presentation"><a href="#shipin" aria-controls="shipin" role="tab" data-toggle="tab"><label>視頻</label>/</a></li>
				 </ul>

				 <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="shuji">
						  <?php
				           query_posts('cat=41&showposts=5&orderby=date&order=asc');
				          ?>
				          <?php while (have_posts()) : the_post(); ?>
				          <div class="reco_yy_block">
					    		<div class="reco_yy_img"><?php the_post_thumbnail();?><span><!-- <img src="<?php bloginfo('template_url');?>/images/m_icon.png"/> --></span></div>
					    		<div class="reco_yy_txt"><h5><a href="<?php the_permalink(); ?>" title="<?php the_title();?>" alt="<?php the_title();?>"><?php the_title();?></a></h5></div>
				    	  </div>
				          <?php endwhile;?>
				          <?php wp_reset_query(); ?>
				    
				    </div>
				    <div role="tabpanel" class="tab-pane" id="yinyue">
				    	<?php
				           query_posts('cat=42&showposts=5&orderby=date&order=asc');
				          ?>
				          <?php while (have_posts()) : the_post(); ?>
				          <div class="reco_yy_block">
					    		<div class="reco_yy_img"><img src="<?php echo pods_field_display('file_download', get_the_ID(), 'file_download_pic')?>"/><span><img src="<?php bloginfo('template_url');?>/images/m_icon.png"/></span></div>
					    		<div class="reco_yy_txt"><h5><a href="<?php echo pods_field_display('file_download', get_the_ID(), 'file_download_link')?>" title="<?php the_title();?>" alt="<?php the_title();?>" target="_blank"><?php the_title();?></a></h5></div>
				    	  </div>
				          <?php endwhile;?>
				          <?php wp_reset_query(); ?>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="shipin">
				    	<?php
				           query_posts('cat=43&showposts=5&orderby=date&order=asc');
				          ?>
				          <?php while (have_posts()) : the_post(); ?>
				          <div class="reco_yy_block">
					    		<div class="reco_yy_img"><img src="<?php echo pods_field_display('file_download', get_the_ID(), 'file_download_pic')?>"/><span><img src="<?php bloginfo('template_url');?>/images/v_icon.png"/></span></div>
					    		<div class="reco_yy_txt"><h5><a href="<?php echo pods_field_display('file_download', get_the_ID(), 'file_download_link')?>" title="<?php the_title();?>" alt="<?php the_title();?>" target="_blank"><?php the_title();?></a></h5></div>
				    	  </div>
				          <?php endwhile;?>
				          <?php wp_reset_query(); ?>
				    </div>
				 </div>
				 <div class="recommend_more">
				 	<a href="/category/shanlirenjia/xiazai/shuji/">查看更多</a>
				 </div>
			</div>
		</div>
	</div>
</div>


<?php get_footer();?>