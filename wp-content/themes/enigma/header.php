<!DOCTYPE html>
<!--[if lt IE 7]>
    <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>
    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>
    <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <meta charset="<?php bloginfo('charset'); ?>" />	
    <meta name="description" content="<?php if(get_field('seo_keywords')!=''){
    	the_field('seo_keywords');
    	}else{
    		if(!is_single()){
    			echo "普利禅寺（又名洞山寺），洞山良价祖师在此创立中国佛教禅宗之一的曹洞宗。近年来在南怀瑾先生的倡议下，普利禅寺完成重修并开放。"; 
    		}else{
    			echo the_title()."|普利禅寺（又名洞山寺）";
    		}
    	} ?>" />
    <meta name="keywords" content="<?php if(get_field('seo_description')!=''){
    	the_field('seo_description');
    	}else{
    		if(!is_single()){
    			echo "洞山寺,普利禅寺,曹洞宗";
    		}else{
    			echo the_title()."|普利禅寺";
    		}
    		
    	} ?>" />
	<?php $wl_theme_options = weblizar_get_options(); ?>
	<?php if($wl_theme_options['upload_image_favicon']!=''){ ?>
	<link rel="shortcut icon" href="<?php  echo esc_url($wl_theme_options['upload_image_favicon']); ?>" /> 
	<?php } ?>
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/jquery.bxslider.css">
	<?php wp_head(); ?>
	<meta name="360-site-verification" content="1c4a12e6cb8d5aa7a667a02d0614212d" />
</head>
<body <?php body_class(); ?>>
	<!-- Header Section -->
	<?php //if(is_home()):?>
	<div class="header_section" >
		<div class="container" >
			<!-- Logo & Multi Language -->
			<!--<div class="row ">
			   <div class="col-md-12 col-sm-12 wl_rtl">
			   	  <div class="header_lang">
			   	     <?php $cur_lang = qtranxf_getLanguage();?>
			   	  	<span class="header_lang_circle"><a href="/?lang=zh" <?php if($cur_lang=='zh'){echo "style='background:#99594c'";}?> >繁</a></span>
			   	  	<span class="header_lang_circle"><a href="/?lang=en" <?php if($cur_lang=='en'){echo "style='background:#99594c'";}?> >EN</a></span>
			   	  </div>
			   </div>
			</div>-->
			<!-- /Logo & Multi Language -->
		</div>	
	</div>	
	<?php //endif;?>
	
	<!-- /Header Section -->
	<!-- Navigation  menus -->
	<div class="navigation_menu "  data-spy="affix" data-offset-top="95" id="enigma_nav_top">
			<nav class="navbar navbar-default " role="navigation">
				<div class="header_img"><a href="<?php echo esc_url(home_url( '/' )); ?>"><img src="<?php bloginfo('template_url');?>/images/logotop.png" alt="普利禪寺" title="普利禪寺"></a></div>
				<div class="menu_center_logo">
					<a href="<?php echo esc_url(home_url( '/' )); ?>">
						<?php if($wl_theme_options['upload_image_logo']){ ?>
							<img src="<?php echo $wl_theme_options['upload_image_logo']; ?>"/>
						<?php } else { ?>
							<img src="<?php bloginfo('template_url');?>/images/logotxt.png"/>
						<?php } ?>
					</a>
				</div>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
					  <span class="sr-only"><?php _e('Toggle navigation');?></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>
				</div>
				<div id="menu" class="collapse navbar-collapse ">	
					<?php  
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class' => 'nav navbar-nav',
							'fallback_cb' => 'weblizar_fallback_page_menu',
							'walker' => new weblizar_nav_walker(),

							)
						);
					?>		
				</div>	
			</nav>
	</div>

	<script type="text/javascript">
			jQuery(function(){
				if(jQuery(window).width()>767){
					jQuery('.dropdown-menu').width(jQuery(window).width());
				}
				jQuery('.dropdown-menu').css('position','fixed');
				var sheight = jQuery(".navigation_menu").offset().top+jQuery(".navigation_menu").height();
				jQuery('.dropdown-menu').css("left",'0').css('top',sheight);

				jQuery(window).scroll(function(){
					//为了保证兼容性，这里取两个值，哪个有值取哪一个　　
					var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
					
					if(scrollTop>=110){
						jQuery('.dropdown-menu').css("left",'0').css('top',90);
					}else{
						jQuery('.dropdown-menu').css("left",'0').css('top',sheight);
					}　
				}); 
			});
	</script>