<?php get_header(); ?>
<?php
	$root_cat_id = get_root_id($cat);   //获取到顶级分类
?>
<div class="clearfix neiye_banner">
	<div class="row">
		<?php if ($root_cat_id==2){?>
			<img src="<?php bloginfo('template_url');?>/images/dongxiazhifeng.jpg"/>
		<?php }else if($root_cat_id==3){?>
			<img src="<?php bloginfo('template_url');?>/images/duyingwudao.jpg"/>
		<?php }else if($root_cat_id==4){?>
			<img src="<?php bloginfo('template_url');?>/images/shanlirenjia.jpg"/>
		<?php }else{?>
			<img src="<?php bloginfo('template_url');?>/images/duyingwudao.jpg"/>
		<?php }?>
	</div>
</div>

<div class="clearfix">
  <div class="container">
    <div class="row">
		<div class="row neiye_warpper">
		    <?php get_sidebar(); ?>	
			<div class="col-md-9 col-sm-9 col-xs-12">
			  <div class="neiye_content">
				<?php 
					if ( have_posts() ) : while ( have_posts() ) : 
						the_post(); 	
					    echo "<h1>";
						the_title();
						echo "</h1>";
						the_content();
					endwhile; endif;
				?>
		       </div>
			</div>
		</div>
	</div>
  </div>
</div>

<script type="text/javascript">
jQuery(function(){
	jQuery(".neiye_content p").each(function(){
		if(jQuery(this).has('img').length){
			jQuery(this).css("text-indent",0);
			jQuery(this).css("width","auto");
		}
	});
});
</script>

<?php get_footer(); ?>