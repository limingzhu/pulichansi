<?php
	 /*
	 Template Name: 洞下之风模板
	 */
?>
<?php get_header();  ?>
<div class="clearfix neiye_banner">
	<div class="row">
		<?php
			if ( has_post_thumbnail() ) { 
			  the_post_thumbnail();
			}else{?>
			<img src="<?php bloginfo('template_url');?>/images/dongxiazhifeng.jpg"/>
			<?php }?>
	</div>
</div>

<div class="clearfix">
  <div class="container">
    <div class="row">
		<div class="row neiye_warpper">
		    <?php get_sidebar(); ?>	
			<div class="col-md-9 col-sm-9 col-xs-12">
			  <div class="dongxia_content">
				<h1><?php the_field('page_big_title');?></h1>
				<?php 
	                while ( have_posts() ) : the_post();
	                   the_content();
	                endwhile;
	             ?>
	             <?php if(is_page(228)): ?>
	             	<div class="dongxia_scene">
		             	<?php 
			             	$number     = 6;  
		                    $paged      = (get_query_var('paged')) ? get_query_var('paged') : 1;  
		                    $offset     = ($paged - 1) * $number;  
		                    $scenes     = get_posts('post_type=mountain_scenic&orderby=id&order=asc&showposts=-1');  
		                    $query      = get_posts('post_type=mountain_scenic&orderby=id&order=asc&offset='.$offset.'&showposts='.$number);  
		                    $total_scenes = count($scenes);  
		                    $total_query  = count($query);  
		                    $total_pages  = intval($total_scenes / $number) + 1; 
		                    foreach($query as $key=>$q) { ?>  
			                    <div class="scene_box ">  
			                        <a href="javascript:0;" class="cd-popup-trigger<?php echo $key;?>">  
			                           <div class="scene_one">
			                              <img src="<?php echo pods_field_display('mountain_scenic', $q->ID, 'mountain_scenic_spic');?>" alt="<?php echo $q->post_title;?> " title="<?php echo $q->post_title;?> "/>
			                              <div class="scene_one_txt">【<?php echo $q->post_title;?>】</div>
			                           </div>
			                        </a>  
			                        <div class="cd-popup cd-popup<?php echo $key;?>">
									    <div class="cd-popup-container">
									        <div class="scene_box_big">
									            <img src="<?php echo pods_field_display('mountain_scenic', $q->ID, 'mountain_scenic_big_pic');?>"  alt="<?php echo $q->post_title;?> " title="<?php echo $q->post_title;?> "/>
									            <div class="scene_box_big_txt">
									            	<h5>【<?php echo $q->post_title;?>】</h5>
									            	<p><?php echo pods_field_display('mountain_scenic', $q->ID, 'mountain_scenic_describe');?></p>
									            </div>
									        </div>
									        <a href="#0" class="scene_box_close cd-popup-close<?php echo $key;?>"></a>
									    </div>
									</div>
			                    </div>  
		                <?php }   ?>

 
		                 <?php  
		                    if ($total_scenes > $total_query) {  
		                        echo '<div id="pagination" class="clearfix">';  
		                        echo '<span class="pages">共'.$total_pages.'页</span>';  
		                        $current_page = max(1, get_query_var('paged'));  
		                        echo paginate_links(array(  
		                            'base' => get_pagenum_link(1) . '%_%',  
		                            'format' => 'page/%#%/',  
		                            'current' => $current_page,  
		                            'total' => $total_pages,  
		                            'prev_next'    => true,  
		                            'type'         => 'list', 
		                            'prev_text'    => __('<'),
									'next_text'    => __('>'), 
		                            ));  
		                        echo '</div>'; 
		                    } 
		                 ?>
		                 <script type="text/javascript">
						    /*弹框JS内容*/
						    jQuery(document).ready(function($){
						    	<?php foreach ($query as $key=>$q) : ?>
						    		$('.cd-popup-trigger<?php echo $key;?>').on('click', function(event){
							            //console.log("lalalal:",<?php echo $key;?>);
							            event.preventDefault();
							            $('.cd-popup<?php echo $key;?>').addClass('is-visible');
							            console.log("addClass",".cd-popup<?php echo $key;?>");
							            //$(".dialog-addquxiao").hide()
								        });
								        //关闭窗口
								        $('.cd-popup<?php echo $key;?>').on('click', function(event){
								            if( $(event.target).is('.cd-popup-close<?php echo $key;?>') || $(event.target).is('.cd-popup<?php echo $key;?>') ) {
								                event.preventDefault();
								                $(this).removeClass('is-visible');
								            }
								        });
								        //ESC关闭
								        $(document).keyup(function(event){
								            if(event.which=='27'){
								                $('.cd-popup<?php echo $key;?>').removeClass('is-visible');
								            }
								        });
								 <?php endforeach;?>
							 });
						  </script>
	             <?php endif;?>
			  </div>
		   </div>
		</div>   
	</div>
   </div>	
</div>
</div>
<?php get_footer(); ?>

<script type="text/javascript">
	jQuery(function(){
		jQuery(".dongxia_content p").each(function(){
			if(jQuery(this).has('img').length){
				jQuery(this).css("text-indent",0);
			}
		});
	})
</script>