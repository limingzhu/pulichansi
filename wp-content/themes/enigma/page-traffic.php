<?php
	 /*
	 Template Name: 交通攻略模板
	 */
?>
<?php get_header();  ?>
<div class="clearfix neiye_banner">
		<div class="row">
		<?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			  the_post_thumbnail();
			}else{   ?>
			<img src="<?php bloginfo('template_url');?>/images/ziguitiigui.jpg"/>
		<?php }?>
	</div>
</div>
<div class="clearfix">
  <div class="container">
    <div class="row">
		<div class="row neiye_warpper">
		    <?php get_sidebar(); ?>	
			<div class="col-md-9 col-sm-9 col-xs-12">
			  <div class="traffic_content">
				 <h1><?php the_field('page_big_title');?></h1>
				 <p><?php the_field('traffic_intro');?></p>
				 <div class="traffic_method">
				 	<h4><label class="traffic_gaotie"></label><?php the_field('traffic_one_title');?></h4>
				 	<p><?php the_field('traffic_one_content');?></p>
				 	<h4><label class="traffic_feiji"></label><?php the_field('traffic_two_title');?></h4>
				 	<p><?php the_field('traffic_two_content');?></p>
				 	<h4><label class="traffic_gaosu"></label><?php the_field('traffic_three_title');?></h4>
				 	<p><?php the_field('traffic_three_content');?></p>
				 </div>
				 <div class="traffic_dongshan">
					 <?php the_field('traffic_yixing_dongshan');?>
				 </div>
			  </div>
		    </div>
		</div>   
	</div>
  </div>	
  <div class="traffic_map" id="traffic_map">
	  
  </div>
</div>
<?php get_footer(); ?>

<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=ETAHdIKgk7HYsekrCx90D1KPNk24FP9k"></script>
<script type="text/javascript"> 
    var sContent ="<h4 style='margin:0 0 5px 0;padding:0.2em 0'><?php the_field('traffic_map_title');?></h4>" + 
	"<img style='float:right;margin:4px' id='imgDemo' src='<?php the_field('traffic_map_pic');?>' width='139' height='104'/>" + 
	"<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'><?php the_field('traffic_map_intro');?></p>" + 
	"</div>";
	var map = new BMap.Map("traffic_map");
	var point = new BMap.Point(114.876532,28.542885);
	map.centerAndZoom(point, 15);
	var infoWindow = new BMap.InfoWindow(sContent);  // 创建信息窗口对象
	map.openInfoWindow(infoWindow,point); //开启信息窗口

	map.addControl(new BMap.NavigationControl());
	map.addControl(new BMap.ScaleControl());    
	map.addControl(new BMap.OverviewMapControl());    
	map.addControl(new BMap.MapTypeControl());         

</script>  