<?php
	 /*
	 Template Name: 南師懷瑾专题模板
	 */
?>
<?php get_header();  ?>

<div class="clearfix neiye_banner">
	<div class="row">
		<?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			  the_post_thumbnail();
			}else{   ?>
			<img src="<?php bloginfo('template_url');?>/images/nanhuanjin.jpg"/>
		<?php }?>
	</div>
</div>

<div class="renwu">

<div class="clearfix renwu_nav_bg">
  <div class="container renwu_nav">
         <?php 
		       wp_nav_menu( array(
				'theme_location' => 'secondary',
				'menu_class' => 'nav navbar-nav',
				'fallback_cb' => 'weblizar_fallback_page_menu',
				'walker' => new weblizar_nav_walker(),
				)
			);	
	     ?>				
  </div>
</div>

<div class="clearfix">
	<div class="container renwu_intro">
		<div class="col-md-12">
			<div class="col-md-1 col-sm-1 col-xs-1 renwu_intro_left">
				<img src="<?php bloginfo('template_url');?>/images/renwu_intro.png"/>
			</div>
			<div class="col-md-11 col-sm-11 col-xs-11 renwu_intro_right">
			<?php
	           query_posts('cat=50&showposts=1&orderby=date&order=ASC');
	         ?>
			 <?php while (have_posts()) : the_post(); ?>		 	
			 	<?php the_excerpt();?>
			 	<a class="renwu_more" href="<?php the_permalink();?>">【查看詳情】</a>
	          <?php endwhile;?>
	         <?php wp_reset_query();?>
			</div>
	   </div>
	</div>
</div>

<div class="clearfix">
   <div class="container renwu_taoli" id="taolifenfang">
	   <div class="renwu_small_header col-md-12">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>怀念南师</h5>
	   </div>
	   <div class="col-md-12 renwu_taoli_top">
		<?php
           query_posts('cat=51&showposts=1&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>
			 <div class="col-md-3 col-sm-3 col-xs-12">
		   		<?php catch_that_image();?>
		     </div>
			 <div class="col-md-9 col-sm-9 col-xs-12">	
				 <h6><a href="<?php the_permalink();?>"><?php the_title();?></a></h6>	 	
			 	<?php the_excerpt();?>
			 	<a class="renwu_more" href="<?php the_permalink();?>">【查看詳情】</a>
	      <?php endwhile;?>
	      <?php wp_reset_query();?> 	
	     </div>
	   </div>
	   <div class="col-md-12 renwu_taoli_list">
	    <?php
           query_posts('cat=51&offset=1&showposts=6&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>	
			 <div class="col-md-2 col-sm-4 col-xs-6">
		   	 	<?php catch_that_image();?>
		   	 	<p><?php the_field('taoli_name');?></p>
		   	 	<label><a href="<?php the_permalink();?>"><?php the_title();?></a></label>
		   	 </div>
          <?php endwhile;?>
         <?php wp_reset_query();?>
	   </div>
   </div>
</div>

<div class="clearfix">
	<div class="container renwu_houshi" id="houshipingshuo">
	   <div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>媒体评论</h5>
	   </div>
	   <div class="col-md-12 renwu_houshi_top">
	     <?php
           query_posts('cat=52&showposts=1&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>
		 	<div class="col-md-2 col-sm-2 col-xs-12"><?php catch_that_image();?></div>
		   <div class="col-md-10 col-sm-10 col-xs-12">
		     <h5><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
		     <?php the_excerpt(); ?>
		   	 <a href="<?php the_permalink();?>" class="renwu_more">【查看詳情】</a>
		   </div>
	      <?php endwhile;?>
	      <?php wp_reset_query();?> 
	   </div>
	   <div class="col-md-12 renwu_houshi_list">
	     <?php
           query_posts('cat=52&offset=1&showposts=4&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>	
			 <div class="col-md-3 col-sm-6 col-xs-12">
		   		<?php catch_that_image();?>
		   		<label><a href="<?php the_permalink();?>"><?php the_title();?></a></label>
		   	</div>
          <?php endwhile;?>
         <?php wp_reset_query();?>
	   </div>
	</div>
</div>

<div class="clearfix">
	<div class="container renwu_gaopeng" id="sihaigaopeng">
		<div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>亲近怀师</h5>
	    </div>
	    <div class="col-md-12">
		    <div class="col-md-5">
		    	<img src="<?php bloginfo('template_url');?>/images/renwu5.jpg"/>
		    </div>
		    <div class="col-md-7 renwu_gaopeng_list">
		    <?php
	           query_posts('cat=53&showposts=5&orderby=date&order=ASC');
	         ?>
			 <?php while (have_posts()) : the_post(); ?>	
				 <div class="renwu_gaopeng_lbox">
			    	<h6><a href="<?php the_permalink();?>"><?php the_title()?></a></h6>
				    <p><?php the_excerpt();?>...<a href="<?php the_permalink();?>" class="renwu_more">【查看详情】</a></p>
				</div>
	          <?php endwhile;?>
	         <?php wp_reset_query();?>
		    </div>
		</div>
	</div>
</div>

<div class="clearfix">
	<div class="container renwu_zhushu" id="zhushudengshen">
	    <div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>著作等身</h5>
	    </div>
	    <div class="col-md-12 renwu_zhushu_list">
         <?php
           query_posts('cat=54&showposts=4&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>	
			 <div class="col-md-3 col-sm-6 col-xs-6 renwu_zhushu_lbox">
			    <?php catch_that_image();?>
			    <a href="<?php the_permalink();?>"><?php the_title();?></a>
		    </div>
          <?php endwhile;?>
         <?php wp_reset_query();?>
	    </div>
	</div>
</div>

<div class="clearfix">
	<div class="container renwu_huiren" id="huirenbujuan">
		 <div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>誨人不倦</h5>
	    </div>
	    <div class="col-md-12">
		     <div class="col-md-5">
		    	<img src="<?php bloginfo('template_url');?>/images/renwu7.jpg"/>
		    </div>
		     <div class="col-md-7 renwu_huiren_list">
		     <?php
	           query_posts('cat=55&showposts=5&orderby=date&order=ASC');
	         ?>
			 <?php while (have_posts()) : the_post(); ?>	
				 <div class="renwu_gaopeng_lbox">
			    	<h6><a href="<?php the_permalink();?>"><?php the_title();?></a></h6>
				    <p><?php the_excerpt();?></p><a href="<?php the_permalink();?>" class="renwu_more">【查看详情】</a>
				</div>
	          <?php endwhile;?>
	         <?php wp_reset_query();?>
		    </div>
		</div>
	</div>
</div>

<div class="clearfix">
	<div class="container renwu_zhaopian" id="zhenguizhaopian">
		<div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>珍貴照片</h5>
	    </div>
	    <div class="col-md-12">
		    <ul class="bxslider">
		    <?php for($i=1;$i<=50;$i++){?>
			  <li><img src="<?php bloginfo('template_url');?>/images/zhaopian/<?php echo $i;?>.jpg" /></li>
			<?php }?>
			</ul>
	    </div>
	</div>
</div>

<div class="clearfix">
	<div class="container renwu_shipin" id="zhenguishipin">
		<div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>珍贵视频</h5>
	    </div>
	    <div class="col-md-12">
			<div class="col-md-9 col-sm-12 col-xs-12">
			    <div class="renwu_shipin_box rshb1 col-md-12 col-sm-12 col-xs-12">
				    <img src="<?php bloginfo('template_url');?>/images/renwu/video_img/shipin1.jpg"/>
				    <div class="renwu_shipin_block">
				    <a href="http://v.qq.com/x/page/d0302oiln4g.html" target="_blank">南師與洞山</a>
				    </div>
				    <div class="renwu_shipin_video">
					    <a href="http://v.qq.com/x/page/d0302oiln4g.html" target="_blank"><img src="<?php bloginfo('template_url');?>/images/video.png"/></a>
				    </div>
			    </div>
			    <div class="renwu_shipin_box rshb2 col-md-4 col-sm-4 col-xs-12">
				    <img src="<?php bloginfo('template_url');?>/images/renwu/video_img/shipin4.jpg"/>
				    <div class="renwu_shipin_block">
				    	<a href="http://v.qq.com/x/cover/h7zmo73qre6gpw6.html?vid=h01265l9jgu" target="_blank">不堪風雨亂紅塵</a>
				    </div>
				    <div class="renwu_shipin_video">
					    <a href="http://v.qq.com/x/cover/h7zmo73qre6gpw6.html?vid=h01265l9jgu" target="_blank"><img src="<?php bloginfo('template_url');?>/images/video.png"/></a>
				    </div>
			    </div>
			    <div class="renwu_shipin_box rshb3 col-md-4 col-sm-4 col-xs-12">
				    <img src="<?php bloginfo('template_url');?>/images/renwu/video_img/shipin5.jpg"/>
				    <div class="renwu_shipin_block">
				    	<a href="http://v.qq.com/x/page/w0117m9yndl.html" target="_blank">怀师荼毗现场</a>
				    </div>
				    <div class="renwu_shipin_video">
					    <a href="http://v.qq.com/x/page/w0117m9yndl.html" target="_blank"><img src="<?php bloginfo('template_url');?>/images/video.png"/></a>
				    </div>
			    </div>
			    <div class="renwu_shipin_box rshb4 col-md-4 col-sm-4 col-xs-12">
				    <img src="<?php bloginfo('template_url');?>/images/renwu/video_img/shipin6.jpg"/>
				    <div class="renwu_shipin_block">
				    	<a href="http://v.qq.com/x/page/p0136zyn6b4.html" target="_blank">遙憶懐師：聚散</a>
				    </div>
				    <div class="renwu_shipin_video">
					    <a href="http://v.qq.com/x/page/p0136zyn6b4.html" target="_blank"><img src="<?php bloginfo('template_url');?>/images/video.png"/></a>
				    </div>
			    </div>
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="renwu_shipin_box rshb5 col-md-12 col-sm-4 col-xs-12">
					<img src="<?php bloginfo('template_url');?>/images/renwu/video_img/shipin2.jpg"/>
					 <div class="renwu_shipin_block">
				    	<a href="http://v.qq.com/x/cover/h7zmo73qre6gpw6.html?vid=p01668biw2c" target="_blank">先生南懷瑾（上）</a>
				    </div>
				    <div class="renwu_shipin_video">
					    <a href="http://v.qq.com/x/cover/h7zmo73qre6gpw6.html?vid=p01668biw2c" target="_blank"><img src="<?php bloginfo('template_url');?>/images/video.png"/></a>
				    </div>
			    </div>
			    <div class="renwu_shipin_box rshb6 col-md-12 col-sm-4 col-xs-12">
				    <img src="<?php bloginfo('template_url');?>/images/renwu/video_img/shipin3.jpg"/>
				    <div class="renwu_shipin_block">
				    	<a href="http://v.qq.com/x/cover/h7zmo73qre6gpw6.html?vid=v0166494sw4" target="_blank">先生南懷瑾（下）</a>
				    </div>
				    <div class="renwu_shipin_video">
					    <a href="http://v.qq.com/x/cover/h7zmo73qre6gpw6.html?vid=v0166494sw4" target="_blank"><img src="<?php bloginfo('template_url');?>/images/video.png"/></a>
				    </div>
			    </div>
			    <div class="renwu_shipin_box rshb7 col-md-12 col-sm-4 col-xs-12">
				    <img src="<?php bloginfo('template_url');?>/images/renwu/video_img/shipin7.jpg"/>
				    <div class="renwu_shipin_block">
				    	<a href="http://v.qq.com/x/page/v0125u6of86.html" target="_blank">古道法師：憶懐師</a>
				    </div>
				    <div class="renwu_shipin_video">
					    <a href="http://v.qq.com/x/page/v0125u6of86.html" target="_blank"><img src="<?php bloginfo('template_url');?>/images/video.png"/></a>
				    </div>
			    </div>
			</div>
		</div>
	</div>
</div>

<div class="clearfix">
	<div class="container" id="dadaochuancheng">
	    <div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>点燃心灯</h5>
	    </div>
		<div class="col-md-12 renwu_dadao">
		 <?php
           query_posts('cat=56&showposts=8&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>	
			 <div class="col-md-3 col-sm-6 col-xs-12">
				<div class="renwu_dadao_box">
					<a href="<?php the_permalink();?>"><?php catch_that_image();?></a>
					<a href="<?php the_permalink();?>"><?php the_title();?></a>
				</div>
			</div>
          <?php endwhile;?>
         <?php wp_reset_query();?>
		</div>
	</div>
</div>

<div class="clearfix">
  <div class="container" id="kuozhanyuedu">
	  <div class="col-md-12 renwu_small_header">
		   	<img src="<?php bloginfo('template_url');?>/images/wsp.png"/><h5>擴展閱讀</h5>
	  </div>
	  <div class="col-md-12">
	   <?php
           query_posts('cat=57&showposts=6&orderby=date&order=ASC');
         ?>
		 <?php while (have_posts()) : the_post(); ?>	
		 <div class="col-md-6">
			  <div class="renwu_kuozhan renwu_floatL">
				  <?php catch_that_image();?>
				  <div class="renwu_k_txt">
				  	<a href="<?php the_permalink();?>"><h5><?php the_title();?></h5></a>
				  	<p><?php the_excerpt();?></p>...
				  	<a href="<?php the_permalink();?>" class="renwu_more">【查看詳情】</a>
				  </div>
			  </div>
		  </div>
          <?php endwhile;?>
         <?php wp_reset_query();?>
	  </div>
  </div>
</div>

</div>

<?php get_footer(); ?>

<script src="<?php bloginfo('template_url');?>/js/jquery.bxslider.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
	  jQuery('.bxslider').bxSlider({
	  	  slideWidth: 250,
	  	  minSlides: 4,
	  	  maxSlides: 4,
	  	  moveSlides: 1,
	  	  slideMargin: 5,
	  });
	});
</script>
<script type="text/javascript">
jQuery("#menu-menu-person li a").click(function(){
	var url = jQuery(this).attr("href");
	var id = url.split("#")[1];
    if(id){
        var t = jQuery("#"+id).offset().top-120;
        jQuery("html,body").animate({scrollTop:t},1000)
    }
    return false;
});
</script>
