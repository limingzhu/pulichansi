<?php
	 /*
	 Template Name: 重建纪实模板
	 */
?>
<?php get_header(); 
//get_template_part('breadcrums'); ?>
<div class="clearfix neiye_banner">
		<div class="row">
		<?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			  the_post_thumbnail();
			}else{   ?>
			<img src="<?php bloginfo('template_url');?>/images/ziguitiigui.jpg"/>
		<?php }?>
	</div>
</div>
<div class="clearfix">
  <div class="container">
    <div class="row">
		<div class="row neiye_warpper">
		    <?php get_sidebar(); ?>	
			<div class="col-md-9 col-sm-9 col-xs-12">
			  <div class="neiye_content">
					<h1><?php the_field('page_big_title');?></h1>
				   <?php 
		                while ( have_posts() ) : the_post();
		                   the_content();
		                endwhile;
	                ?>
              </div>
			</div>
		</div>   
	</div>
   </div>	
</div>
<?php get_footer(); ?>

<script type="text/javascript">
	jQuery(function(){
		console.log(jQuery(".neiye_warpper p"));
		jQuery(".neiye_content p").each(function(){
			if(jQuery(this).has('img').length){
				jQuery(this).css("text-indent",0);
			}
		});
	})
</script>