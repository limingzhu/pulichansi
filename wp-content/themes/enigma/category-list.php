<?php if(is_category(12)||is_category(16)||is_category(17)||is_category(18)||is_category(19)||is_category(10)):       //活动预告，易家，儒家，道家，雜家,曹洞宗門?>
<div class="neiye_hdyg">
  <div class="neiye_title"><?php $thiscat = get_category($cat); echo $thiscat ->name;?></div>
  <ul class="neiye_list_ul">
      <?php while (have_posts()) : the_post(); ?>      
        <li>
          <label>·</label>
          <span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><label><?php the_time('Y-m-d'); ?></label></span>
        </li>
      <?php endwhile;?>
  </ul>
  <?php  weblizar_pagination();?>
</div>
<?php endif;?>


<?php if(is_category(13)):       //成果展示?>
<div class="neiye_cgzs">
  <?php while (have_posts()) : the_post(); ?>    
    <div class="neiye_cgzs_box col-md-6 col-sm-6 col-xs-12">
      <div class="cgzs_box">
       <?php catch_that_image();?>
       <div class="cgzs_txt">
         <h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
         <label><?php the_time('Y-m-d'); ?></label>
       </div>
      </div>
    </div>
  <?php endwhile;?>
  <?php  weblizar_pagination();?>
</div>
<?php endif;?>


<?php if(is_category(20)||get_category($cat)->category_parent==20):    //佛教历史?>
<div class="neiye_fjls">
  <div class="neiye_title">
    <?php $thiscat = get_category(20); echo $thiscat->name;?>
    <div class="neiye_sec_title">
      <a <?php if($cat==20){echo "class='on'";} ?> href="<?php echo get_category_link(20);?>">全部</a>
      <?php 
        $categories = get_categories(array('parent' => 20,'hide_empty' => 0,'orderby'=>term_id,'order'=>asc));
        foreach ($categories as $key => $value) {
          if($cat == $value->term_id){
            echo "<a class='on' href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }else{
            echo "<a href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }
        }
      ?>
    </div>   
  </div>
  <ul class="neiye_list_ul">
      <?php while (have_posts()) : the_post(); ?>      
        <li>
          <label>·</label>
          <?php  $category = get_the_category();?>
          <span class="cat_span"><?php if($category[0]){echo '<a href="'.get_category_link($category[0]->term_id ).'" title="'.$category[0]->cat_name.'">【'.$category[0]->cat_name.'】</a>';}?>
          </span>
          <span class="title_span">
           <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><label><?php the_time('Y-m-d'); ?></label></span>
        </li>
      <?php endwhile;?>
  </ul>
  <?php  weblizar_pagination();?>
</div>
<?php endif;?>


<?php if(is_category(21)||get_category($cat)->category_parent==21):    //佛教理论?>
<div class="neiye_fjls">
  <div class="neiye_title">
    <?php $thiscat = get_category(21); echo $thiscat->name;?>
    <div class="neiye_sec_title">
      <a <?php if($cat==21){echo "class='on'";} ?> href="<?php echo get_category_link(21);?>">全部</a>
      <?php 
        $categories = get_categories(array('parent' => 21,'hide_empty' => 0,'orderby'=>term_id,'order'=>asc));
        foreach ($categories as $key => $value) {
          if($cat == $value->term_id){
            echo "<a class='on' href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }else{
            echo "<a href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }
        }
      ?>
    </div>   
  </div>
  <ul class="neiye_list_ul">
      <?php while (have_posts()) : the_post(); ?>      
        <li>
          <label>·</label>
          <?php  $category = get_the_category();?>
          <span class="cat_span"><?php if($category[0]){echo '<a href="'.get_category_link($category[0]->term_id ).'" title="'.$category[0]->cat_name.'">【'.$category[0]->cat_name.'】</a>';}?>
          </span>
          <span class="title_span">
           <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><label><?php the_time('Y-m-d'); ?></label></span>
        </li>
      <?php endwhile;?>
  </ul>
  <?php  weblizar_pagination();?>
</div>
<?php endif;?>

<?php if(is_category(22)):    //佛法与科学?>
<div class="neiye_fjls">
  <div class="neiye_title">
    <?php $thiscat = get_category(22); echo $thiscat->name;?>
  </div>
  <ul class="neiye_list_ul">
      <?php while (have_posts()) : the_post(); ?>      
        <li>
          <label>·</label>
          <?php  $category = get_the_category();?>
          <span class="cat_span"><?php if($category[0]){echo '<a href="'.get_category_link($category[0]->term_id ).'" title="'.$category[0]->cat_name.'">【'.$category[0]->cat_name.'】</a>';}?>
          </span>
          <span class="title_span">
           <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><label><?php the_time('Y-m-d'); ?></label></span>
        </li>
      <?php endwhile;?>
  </ul>
  <?php  weblizar_pagination();?>
</div>
<?php endif;?>

<?php if(is_category(23)||get_category($cat)->category_parent==23):    //解脱之路?>
<div class="neiye_fjls">
  <div class="neiye_title">
    <?php $thiscat = get_category(23); echo $thiscat->name;?>
    <div class="neiye_sec_title">
      <a <?php if($cat==23){echo "class='on'";} ?> href="<?php echo get_category_link(23);?>">全部</a>
      <?php 
        $categories = get_categories(array('parent' => 23,'hide_empty' => 0,'orderby'=>term_id,'order'=>asc));
        foreach ($categories as $key => $value) {
          if($cat == $value->term_id){
            echo "<a class='on' href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }else{
            echo "<a href=".get_category_link($value->term_id).">".$value->cat_name."</a>";
          }
        }
      ?>
    </div>   
  </div>
  <ul class="neiye_list_ul">
      <?php while (have_posts()) : the_post(); ?>      
        <li>
          <label>·</label>
          <?php  $category = get_the_category();?>
          <span class="cat_span"><?php if($category[0]){echo '<a href="'.get_category_link($category[0]->term_id ).'" title="'.$category[0]->cat_name.'">【'.$category[0]->cat_name.'】</a>';}?>
          </span>
          <span class="title_span">
           <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><label><?php the_time('Y-m-d'); ?></label></span>
        </li>
      <?php endwhile;?>
  </ul>
  <?php  weblizar_pagination();?>
</div>
<?php endif;?>





<?php 
    $defaults = array(
            'before'           => '<div id="pagination"><ul class="page-numbers">' . __( 'Pages:'),
            'after'            => '</ul></div>',
            'link_before'      => true,
            'link_after'       => true,
            'next_or_number'   => 'number',
            'separator'        => ' ',
            'nextpagelink'     => __( 'Next page'),
            'previouspagelink' => __( 'Previous page'),
            'pagelink'         => '%',
            'echo'             => 1
            );
    wp_link_pages( $defaults );
?>